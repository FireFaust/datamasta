#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T16:29:26
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
CONFIG   += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MarkerTest
TEMPLATE = app


SOURCES += main.cpp\
        markertest.cpp \
        writer.cpp \
        smartzoomableview.cpp \
        flycap.cpp \
    aiprocess.cpp \
    rectselectscene.cpp \
    colormanager.cpp \
    processor.cpp \
    worker.cpp

HEADERS  += markertest.h \
    writer.h \
    smartzoomableview.h \
    flycap.h \
    aiprocess.h \
    rectselectscene.h \
    colormanager.h \
    processor.h \
    worker.h

FORMS    += markertest.ui

flycap {
    DEFINES += HAS_FLYCAP
}

LIBS += -lflycapture

TEMPLATE = app

LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

#INCLUDEPATH += /home/rotjix/ffmpeg_build/include
LIBS += -L/usr/lib
#LIBS += -L/home/rotjix/ffmpeg_build/lib

#LIBS += -lavdevice -lva -lva-x11 -lva -lxcb -lXau -lXdmcp -lxcb-shm -lxcb -lXau -lXdmcp -lxcb-xfixes -lxcb -lXau -lXdmcp -lxcb-render -lxcb-shape -lxcb-shape -lxcb -lXau -lXdmcp -lX11 -lasound -lSDL -lpthread -lm -ldl -lasound -lpulse-simple -lpulse -lX11 -lXext -lcaca -L/home/rotjix/ffmpeg_build/lib -lx265 -lstdc++ -lm -lrt -ldl -L/usr/local/lib -lx264 -lpthread -lm -ldl -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -lvorbisenc -lvorbis -logg -ltheoraenc -ltheoradec -logg -lopus -lm -lmp3lame -lfreetype -lz -lpng12 -L/home/rotjix/ffmpeg_build/lib -lfdk-aac -lm -lass -lharfbuzz -lfontconfig -lfreetype -lexpat -lenca -lm -lfribidi -lz -lpng12 -lm -llzma -lz -pthread
#LIBS += -lavfilter -lva -lva-x11 -lva -lxcb -lXau -lXdmcp -lxcb-shm -lxcb -lXau -lXdmcp -lxcb-xfixes -lxcb -lXau -lXdmcp -lxcb-render -lxcb-shape -lxcb-shape -lxcb -lXau -lXdmcp -lX11 -lasound -lSDL -lpthread -lm -ldl -lasound -lpulse-simple -lpulse -lX11 -lXext -lcaca -L/home/rotjix/ffmpeg_build/lib -lx265 -lstdc++ -lm -lrt -ldl -L/usr/local/lib -lx264 -lpthread -lm -ldl -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -lvorbisenc -lvorbis -logg -ltheoraenc -ltheoradec -logg -lopus -lm -lmp3lame -lfreetype -lz -lpng12 -L/home/rotjix/ffmpeg_build/lib -lfdk-aac -lm -lass -lharfbuzz -lfontconfig -lfreetype -lexpat -lenca -lm -lfribidi -lz -lpng12 -lm -llzma -lz -pthread
#LIBS += -lavcodec -lva -lva-x11 -lva -lxcb -lXau -lXdmcp -lxcb-shm -lxcb -lXau -lXdmcp -lxcb-xfixes -lxcb -lXau -lXdmcp -lxcb-render -lxcb-shape -lxcb-shape -lxcb -lXau -lXdmcp -lX11 -lasound -lSDL -lpthread -lm -ldl -lasound -lpulse-simple -lpulse -lX11 -lXext -lcaca -L/home/rotjix/ffmpeg_build/lib -lx265 -lstdc++ -lm -lrt -ldl -L/usr/local/lib -lx264 -lpthread -lm -ldl -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -lvorbisenc -lvorbis -logg -ltheoraenc -ltheoradec -logg -lopus -lm -lmp3lame -lfreetype -lz -lpng12 -L/home/rotjix/ffmpeg_build/lib -lfdk-aac -lm -lass -lharfbuzz -lfontconfig -lfreetype -lexpat -lenca -lm -lfribidi -lz -lpng12 -lm -llzma -lz -pthread
#LIBS += -lavformat -lva -lva-x11 -lva -lxcb -lXau -lXdmcp -lxcb-shm -lxcb -lXau -lXdmcp -lxcb-xfixes -lxcb -lXau -lXdmcp -lxcb-render -lxcb-shape -lxcb-shape -lxcb -lXau -lXdmcp -lX11 -lasound -lSDL -lpthread -lm -ldl -lasound -lpulse-simple -lpulse -lX11 -lXext -lcaca -L/home/rotjix/ffmpeg_build/lib -lx265 -lstdc++ -lm -lrt -ldl -L/usr/local/lib -lx264 -lpthread -lm -ldl -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -L/home/rotjix/ffmpeg_build/lib -lvpx -lm -lpthread -lvorbisenc -lvorbis -logg -ltheoraenc -ltheoradec -logg -lopus -lm -lmp3lame -lfreetype -lz -lpng12 -L/home/rotjix/ffmpeg_build/lib -lfdk-aac -lm -lass -lharfbuzz -lfontconfig -lfreetype -lexpat -lenca -lm -lfribidi -lz -lpng12 -lm -llzma -lz -pthread
#LIBS += -lavutil -lm
#LIBS += -lpostproc
#LIBS += -lswresample -lm
#LIBS += -lswscale -lm

LIBS += \
    -lavutil \
    -lavfilter \
    -lavcodec \
    -lavformat \
    -lavdevice \
    -lswscale \
    -lswresample \
    -lavfilter \
    -lpostproc

#LIBS += \
#     -lavformat -lavcodec -lavdevice -lavfilter -lavutil -lswscale -lswresample -lpostproc  \
#    -lvorbisenc -lfdk-aac -lmp3lame	 -lvpx -lx265 -ltheora -ltheoradec  -lxcb -lxcb-xfixes -lxcb-shm -lSDL -lX11 -lz \
#    -lass -lfreetype -lopus -lvorbis  -lx264 -lva -lz  -lasound -ldl -fPIC -lm -ldl



 #   -lavformat \
 #   -lavcodec \
 #   -lswresample \
 #   -lavutil \
 #   -lswscale \
 #   -lavdevice \
 #   -lpostproc \
 #   -lavfilter \


# liconv -lxcb -lxcb_shm -lxcb-xfixes -lbxcb-shape

#   -lz -lxcb -lfdk-aac -lmp3lame -lopus -lvorbis -lvpx -lx265 -ldl -fPIC \
 #   -lx264 -lmp3lame -lass -lopencore-amrnb -lopencore-amrwb -ltheora -lfreetype -lSDL -lasound -lvorbisenc
