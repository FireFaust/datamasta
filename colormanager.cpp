#include "colormanager.h"

ColorManager::ColorManager()
{
    setDefaultsColors();
}

ColorManager::~ColorManager()
{

}

void ColorManager::selectColor(qint64 id)
{
    int colorPosition = 20;
    for (int i=0; i<20;i++)
    {
        if(carColorUnion[i].first==-1)
        {
            carColorUnion[i].first = id;
            for(int j=0; j< 20; j++)
            {
                if(colorInUse[j]==-1)
                {
                    colorPosition =j;
                    colorInUse[j] = colorPosition;
                    break;
                }
            }
            carColorUnion[i].second = colorPosition;
            break;
        }
    }
    giveColor(colors[colorPosition]);
}

void ColorManager::returnColor(QPair<qint64, QColor> pair)
{
    qint64 id=-1;
    for (int i=0; i< 21; i++)
    {
        if(colors[i]==pair.second)
        {
            id=i;
            break;
        }
    }
    if(id>=0)
    {
        int colorPosition = id;
        for (int i=0; i<20;i++)
        {
            if(carColorUnion[i].first==-1)
            {
                carColorUnion[i].first = pair.first;
                for(int j=0; j< 20; j++)
                {
                    if(colorInUse[j]==-1)
                    {
                        colorInUse[j] = colorPosition;
                        break;
                    }
                }
                carColorUnion[i].second = colorPosition;
                break;
            }
        }
    }
}

QColor ColorManager::getColorById(qint64 id)
{
    for (int i=0; i< 20; i++)
    {
        if(carColorUnion[i].first==id)
        {
            return colors[carColorUnion[i].second];
        }
    }
    return colors[20];
}

void ColorManager::removeColor(qint64 id)
{
    for(int i=0; i<20;i++)
    {
        if(carColorUnion[i].first==id)
        {
            carColorUnion[i].first=-1;
            carColorUnion[i].second=-1;
            colorInUse[i]=-1;
        }
    }
}

void ColorManager::setDefaultsColors()
{
    colors[0]  = QColor(0,0,0,_normal);
    colors[1]  = QColor(0,0,255,_normal);
    colors[2]  = QColor(0,255,0,_normal);
    colors[3]  = QColor(255,100,255,_normal);
    colors[4]  = QColor(50,100,0,_normal);
    colors[5]  = QColor(80,100,150,_normal);
    colors[6]  = QColor(150,100,50,_normal);
    colors[7]  = QColor(150,0,80,_normal);
    colors[8]  = QColor(150,150,150,_normal);
    colors[9]  = QColor(255,255,0,_normal);
    colors[10] = QColor(0,255,255,_normal);
    colors[11] = QColor(255,0,255,_normal);
    colors[12] = QColor(255,128,0,_normal);
    colors[13] = QColor(255,0,128,_normal);
    colors[14] = QColor(100,255,255,_normal);
    colors[15] = QColor(210,200,100,_normal);
    colors[16] = QColor(210,50,90,_normal);
    colors[17] = QColor(112,112,112,_normal);
    colors[18] = QColor(120,0,0,_normal);
    colors[19] = QColor(0,80,0,_normal);
    colors[20] = QColor(82,0,82,_normal);

    lightColors[0]  = QColor(0,0,0,_bright);
    lightColors[1]  = QColor(0,0,255,_bright);
    lightColors[2]  = QColor(0,255,0,_bright);
    lightColors[3]  = QColor(255,100,255,_bright);
    lightColors[4]  = QColor(50,100,0,_bright);
    lightColors[5]  = QColor(80,100,150,_bright);
    lightColors[6]  = QColor(150,100,50,_bright);
    lightColors[7]  = QColor(150,0,80,_bright);
    lightColors[8]  = QColor(150,150,150,_bright);
    lightColors[9]  = QColor(255,255,0,_bright);
    lightColors[10] = QColor(0,255,255,_bright);
    lightColors[11] = QColor(255,0,255,_bright);
    lightColors[12] = QColor(255,128,0,_bright);
    lightColors[13] = QColor(255,0,128,_bright);
    lightColors[14] = QColor(100,255,255,_bright);
    lightColors[15] = QColor(210,200,100,_bright);
    lightColors[16] = QColor(210,50,90,_bright);
    lightColors[17] = QColor(112,112,112,_bright);
    lightColors[18] = QColor(120,0,0,_bright);
    lightColors[19] = QColor(0,80,0,_bright);
    lightColors[20] = QColor(82,0,82,_bright);

    darkColors[0]  = QColor(0,0,0,_dark);
    darkColors[1]  = QColor(0,0,255,_dark);
    darkColors[2]  = QColor(0,255,0,_dark);
    darkColors[3]  = QColor(255,100,255,_dark);
    darkColors[4]  = QColor(50,100,0,_dark);
    darkColors[5]  = QColor(80,100,150,_dark);
    darkColors[6]  = QColor(150,100,50,_dark);
    darkColors[7]  = QColor(150,0,80,_dark);
    darkColors[8]  = QColor(150,150,150,_dark);
    darkColors[9]  = QColor(255,255,0,_dark);
    darkColors[10] = QColor(0,255,255,_dark);
    darkColors[11] = QColor(255,0,255,_dark);
    darkColors[12] = QColor(255,128,0,_dark);
    darkColors[13] = QColor(255,0,128,_dark);
    darkColors[14] = QColor(100,255,255,_dark);
    darkColors[15] = QColor(210,200,100,_dark);
    darkColors[16] = QColor(210,50,90,_dark);
    darkColors[17] = QColor(112,112,112,_dark);
    darkColors[18] = QColor(120,0,0,_dark);
    darkColors[19] = QColor(0,80,0,_dark);
    darkColors[20] = QColor(82,0,82,_dark);

    for (int i=0; i<20; i++)
    {
        QPair<qint64,qint64> pair(-1,-1);
        carColorUnion[i] = pair;
        colorInUse[i] = -1;
    }

}
