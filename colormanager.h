#ifndef COLORMANAGER_H
#define COLORMANAGER_H

#include <QObject>
#include <QColor>
#include <QMap>

class ColorManager:public QObject
{
    Q_OBJECT
public:
    explicit ColorManager();
    QColor getColorById(qint64);
    ~ColorManager();
private:
    QMap<qint64, QColor> colors;
    QMap<qint64, QColor> lightColors;
    QMap<qint64, QColor> darkColors;
    QPair<qint64,qint64> carColorUnion[20];
    qint64 colorInUse[20];
    void setDefaultsColors();
    const int _bright = 40;
    const int _normal = 80;
    const int _dark = 150;
public slots:
    void selectColor(qint64);
    void removeColor(qint64);
    void returnColor(QPair<qint64,QColor>);
signals:
    void giveColor(QColor);
};

#endif // COLORMANAGER_H
