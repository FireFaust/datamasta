#include "flycap.h"

FlyCap::FlyCap(QObject *parent)
{
    construct();
}

void FlyCap::construct()
{
    connected = false;
}

FlyCap::~FlyCap()
{
    disconnectCam();
}

bool FlyCap::connectCam()
{
    if(isConnected()) return true;
    disconnectCam();

    FlyCapture2::Error error;

    FlyCapture2::BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// GetNumOfCameras: " << error.GetDescription();
        return false;
    }

    qDebug() << "CameraIface// Number of cameras detected: " << numCameras;
    if(numCameras < deviceId+1){
        qWarning() << "CameraIface// Camera ID (" << deviceId << ") < availabele cameras (" << numCameras << ") + 1 !";
        return false;
    }

    FlyCapture2::PGRGuid guid;
    error = busMgr.GetCameraFromIndex(deviceId, &guid);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// " << error.GetDescription();
        return false;
    }

    // Connect the camera
    error = camera.Connect( &guid );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "CameraIface// Connect: Failed to connect to FlyCap camera";
        return false;
    }

    if(!applySettings()) return false;
    error = camera.StartCapture();
    if ( error == FlyCapture2::PGRERROR_ISOCH_BANDWIDTH_EXCEEDED )
    {
        qWarning() << "CameraIface// StartCapture: Bandwidth exceeded from FlyCap camera";
        return false;
    }
    else if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "CameraIface// StartCapture: Failed to start image capture from FlyCap camera";
        return false;
    }

    connected = true;

    return true;
}

bool FlyCap::applySettings()
{
    FlyCapture2::Error error;
    // Get max resolution from mode info
    FlyCapture2::Format7Info format7Info;
    bool format7Supported;
    error = camera.GetFormat7Info(&format7Info,&format7Supported);
    if (error != FlyCapture2::PGRERROR_OK || !format7Supported) {
        qWarning() << "CameraIface// GetFormat7Info: " << error.GetDescription();
        return false;
    }
    FlyCapture2::Format7ImageSettings * f7s = new FlyCapture2::Format7ImageSettings();

    FlyCapture2::CameraInfo camInfo;
    error = camera.GetCameraInfo(&camInfo);

    /// Check if camera is colored
    isColored = camInfo.isColorCamera;

    if(isColored)
    {
        f7s->mode = FlyCapture2::MODE_0;
        f7s->pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW16;
//        f7s->pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW8;
    }
    else
    {
        f7s->mode = FlyCapture2::MODE_0;
        f7s->pixelFormat = FlyCapture2::PIXEL_FORMAT_MONO8;
    }

    f7s->offsetX = 0;
    f7s->offsetY = 0;

    switch(f7s->mode)
    {
    case FlyCapture2::MODE_0:
        f7s->width = format7Info.maxWidth;
        f7s->height = format7Info.maxHeight;
        break;
    case FlyCapture2::MODE_4:
        f7s->width = format7Info.maxWidth/2;
        f7s->height = format7Info.maxHeight/2;
        break;
    default:
        f7s->width = format7Info.maxWidth;
        f7s->height = format7Info.maxHeight;
    }
    //qDebug() << f7s->width << "x" << f7s->height;

    resolution.setWidth(f7s->width);
    resolution.setHeight(f7s->height);

    FlyCapture2::Format7PacketInfo f7pi;
    bool settingsOk;
    error = camera.ValidateFormat7Settings(f7s,&settingsOk,&f7pi);
    if (error != FlyCapture2::PGRERROR_OK || !settingsOk) {
        qWarning() << "CameraIface// Format7Settings invalid: " << error.GetDescription() <<settingsOk;
        return false;
    }
    error = camera.SetFormat7Configuration(f7s , 100.f);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// SetFormat7Configuration: " << error.GetDescription() ;
        return false;
    }

    // Get current trigger settings
    FlyCapture2::TriggerMode triggerMode;
    error = camera.GetTriggerMode( &triggerMode );
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// GetTriggerMode: " << error.GetDescription();
        return false;
    }

    // Set camera to trigger mode 0
    triggerMode.onOff = false;
    triggerMode.mode = 0;
    triggerMode.parameter = 0;
    triggerMode.source = 0;
    error = camera.SetTriggerMode( &triggerMode );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "CameraIface// Failed to set trigger mode for FlyCap camera";
        return false;
    }

    // Get the camera configuration
    FlyCapture2::FC2Config fcconfig;
    error = camera.GetConfiguration(&fcconfig);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// GetConfiguration: " << error.GetDescription() ;
        return false;
    }

    fcconfig.grabTimeout = 4000;
//    fcconfig.numBuffers = 2;

    // Set the camera configuration
    error = camera.SetConfiguration(&fcconfig);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << "CameraIface// SetConfiguration: " << error.GetDescription();
        return false;
    }


    qDebug() << "Config ok";

    return true;
}

void FlyCap::disconnectCam()
{
    if(!camera.IsConnected()) return;
    connected = false;
    FlyCapture2::Error error = camera.StopCapture();
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "FlyCap camera prematurely disconnected";
    }
    camera.Disconnect();
}

unsigned char * FlyCap::getFrame()
{
    FlyCapture2::Image rawImage;
    FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );

    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "Capture failed from FlyCap camera";
        error.PrintErrorTrace();
        return nullptr;
    }
    unsigned char * result;
    if(isColored){
        result = new unsigned char[rawImage.GetRows()*rawImage.GetCols()*3]();
    }else{
        result = new unsigned char[rawImage.GetRows()*rawImage.GetCols()*1]();
    }
    memcpy(result, rawImage.GetData(),  rawImage.GetDataSize() );
    rawImage.ReleaseBuffer();
    return result;
}

bool FlyCap::getResolution(QRect *resolution)
{
    if(!connected) return false;
    *resolution = this->resolution;
    return true;
}
