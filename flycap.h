#ifndef FLYCAP_H
#define FLYCAP_H

#include <QDebug>

#include "flycapture/FlyCapture2.h"

#include <QMutex>
#include <QMutexLocker>

#include "opencv2/imgproc/imgproc.hpp"
#include <QRect>
#include <QMetaMethod>


class FlyCap
{
public:

    FlyCap(QObject *parent = 0);
    ~FlyCap();

    bool connectCam();
    void disconnectCam();
    inline bool isConnected(){return connected;}

    unsigned char* getFrame();
    bool getResolution(QRect *resolution);

    inline void setDevId(int mDevId){deviceId = mDevId;}
    inline void setFilePath(QString path) {path ="";}

private:
    void construct();

    FlyCapture2::Camera camera;
    QRect resolution;
    unsigned int deviceId;

    bool applySettings();
    bool isColored;
    bool connected;
};

#endif // FLYCAP_H
