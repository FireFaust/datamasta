#include "markertest.h"
#include "ui_markertest.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <QImage>
#include <QPixmap>
#include <QIcon>

#include <QBuffer>
#include <QMetaObject>
#include <QCoreApplication>
#include <QObject>

using namespace cv;

FlycapRecorder::FlycapRecorder(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FlycapRecorder)
{
    ui->setupUi(this);

    qRegisterMetaType<uchar *>("uchar *");
    qRegisterMetaType<myMap>("myMap");
    qRegisterMetaType<DataContainer>("DataContainer");
    qRegisterMetaType<ImageData>("ImageData");
    qRegisterMetaType<ResultData>("ResultData");

    camera.setDevId(0);
    camera.connectCam();

    qDebug() << "Connected" << camera.isConnected();
    camera.getResolution(&res);

    writer.moveToThread(&writerThread);
    writerThread.start();

    processor.moveToThread(&processorThread);
    processorThread.start();

    scene = new RectSelectScene();
    ui->preview->setScene(scene);

    QObject::connect(&writer, SIGNAL(finished()), &writerThread, SLOT(quit()),Qt::DirectConnection);
    QObject::connect(&writer, SIGNAL(frameProcessed()), this, SLOT(frameProcessed()));
    QObject::connect(ui->btnStop, &QPushButton::clicked, this, &FlycapRecorder::stopClicked);
    QObject::connect(ui->btnStart, &QPushButton::clicked, this, &FlycapRecorder::btnStartClicked);
    QObject::connect(ui->btnReconnect, &QPushButton::clicked, this, &FlycapRecorder::btnReconnectClicked);
    QObject::connect(scene,SIGNAL(rectangleReady(qint64)),&colorManager,SLOT(selectColor(qint64)));
    QObject::connect(scene,SIGNAL(rectangleDeleted(qint64)),&colorManager,SLOT(removeColor(qint64)));
    QObject::connect(scene,SIGNAL(colorReturned(QPair<qint64,QColor>)),&colorManager,SLOT(returnColor(QPair<qint64,QColor>)));
    QObject::connect(&colorManager,SIGNAL(giveColor(QColor)),scene,SLOT(setNewRect(QColor)));
    QObject::connect(&processor,SIGNAL(sendRect(qint64,QRectF)),scene,SLOT(receiveRect(qint64,QRectF)));
    QObject::connect(&processor,SIGNAL(badRect_CV(qint64)),this,SLOT(receiveBadRect(qint64)));
    QObject::connect(&processor,SIGNAL(badRect_HV(qint64)),this,SLOT(receiveBadRect(qint64)));
    QObject::connect(scene,SIGNAL(carMoved(qint64,double)),this,SLOT(receiveFinalResult(qint64,double)));
    writer.setup(res.width(),res.height(),"/home/rotjix/fl.mp4");

    frame = 0;
    lastSec = QDateTime::currentMSecsSinceEpoch();
    framesInSec = 0;
    previewCounter = 0;

    framesProcessing = 0;
    isRunning = true;
    isRecording = false;
    cameraOnFirstTime = false;

    //scene = new QGraphicsScene();

    tPixmapItem = 0;

    setNoRect();
    btnSignalMapper = new QSignalMapper(this);
    delSignalMapper = new QSignalMapper(this);
    layout = new QVBoxLayout;
    widget = new QWidget;
    layout->setAlignment(Qt::AlignTop);
    state = NEUTRAL;
    connect(btnSignalMapper, SIGNAL(mapped(int)), this, SLOT(buttonClicked(int)));
    connect(delSignalMapper, SIGNAL(mapped(int)), this, SLOT(delButtonClicked(int)));
    QTimer::singleShot(1,this,&FlycapRecorder::grabframe);
    ui->btnStart->setEnabled(false);
    ui->btnSetImg->setEnabled(false);

    _warning.setBrush(QPalette::Button,Qt::yellow);
    _warning.setBrush(QPalette::ButtonText,Qt::black);

    _critical.setBrush(QPalette::Button,Qt::red);
    _critical.setBrush(QPalette::ButtonText,Qt::white);

    _normal.setBrush(QPalette::Button,QColor(240,240,240,255));
    _normal.setBrush(QPalette::ButtonText,Qt::black);

//    cv::namedWindow("image",2);
//    cv::namedWindow("image2",2);
}

FlycapRecorder::~FlycapRecorder()
{
    isRunning = false;
    qDebug() << "done" << writerThread.isRunning();
    if(writerThread.isRunning()) writerThread.wait();
    qDebug() << "done2" << writerThread.isRunning();

    delete ui;
}

void FlycapRecorder::receiveFinalResult(qint64 id, double dist)
{
    bool exist = false;
    for(auto it = widgets.begin(); it!=widgets.end();++it)
    {
        if(it.key()==id)
            exist = true;
    }
    if(!exist) return;

    //widgets[id]->hide();
    if(dist>=12)
        labelsContainer[id]->setText(">11");
    else
        labelsContainer[id]->setText(QString::number(dist, 'f',2));
    if(dist==0)
        buttonsContainer[id]->setPalette(_normal);
    else
    {
        if(dist<=border)
            buttonsContainer[id]->setPalette(_warning);
        else
            buttonsContainer[id]->setPalette(_critical);
    }
}

void FlycapRecorder::setNoRect()
{
    QRectF rect;
    rect.setX(0);
    rect.setY(0);
    rect.setWidth(1);
    rect.setHeight(1);
    scene->setCanResize(false);
    scene->setRect(rect,true);
}

void FlycapRecorder::btnStartClicked()
{
    if(state==NEUTRAL)
    {
        isRecording = !isRecording;
        if(isRecording){
            ui->btnStart->setText("Pause");
        }else{
            ui->btnStart->setText("Resume");
        }
    }
    if(state==PREPARE)
    {
        scene->saveAll();
        saveButtons();
        state=NEUTRAL;
        formatButtons();
        setNoRect();
        //QMetaObject::invokeMethod(&processor,"addInitialImage", Qt::BlockingQueuedConnection, Q_ARG(myMap, scene->getImageRects(true)));
    }
}

void FlycapRecorder::saveButtons()
{
    /// remove not existing buttons --->
    QList<qint64> ids = scene->getIds();
//    if(ids.size()<widgets.size())
//    {
        QList<int> toDelete;
        for(auto iit = widgets.begin();iit!=widgets.end();++iit)
        {
            bool exist = false;
            for (auto it = ids.begin();it<ids.end();++it)
            {
                if(iit.key()==(*it))
                {
                    exist = true;
                    break;
                }
            }
            if(!exist) toDelete.push_back(iit.key());
        }
        for (auto it = toDelete.begin();it<toDelete.end();++it)
        {
            delButtonClicked(*it);
        }
//    }
    /// remove not existing buttons <---
    /// add new buttons --->
    for (auto it = ids.begin();it<ids.end();++it)
    {
        bool canInsert = true;
        for(auto iit = widgets.begin();iit!=widgets.end();++iit)
        {
            if(iit.key()==(*it))
            {
                canInsert = false;
                break;
            }
        }
        if(!canInsert) continue;
        QPalette pal;
        pal.setBrush(QPalette::Button,colorManager.getColorById((*it)));
        pal.setBrush(QPalette::ButtonText,Qt::white);
        QPushButton *button = new QPushButton(QString("car_"+QString::number((*it))));
        QPushButton *del    = new QPushButton(QString("x"));
        QLabel *label       = new QLabel();
        labelsContainer[(*it)] = label;
        buttonsContainer[(*it)] = button;
        del->setPalette(pal);
        del->setMaximumWidth(20);
        carList.push_back(button);
        btnSignalMapper->setMapping(button, (*it));
        delSignalMapper->setMapping(del,(*it));
        connect(button, SIGNAL(clicked()), btnSignalMapper, SLOT(map()));
        connect(del, SIGNAL(clicked()), delSignalMapper, SLOT(map()));
        QHBoxLayout *hLayout = new QHBoxLayout();
        hLayout->addWidget(buttonsContainer[(*it)]);
        hLayout->addWidget(del);
        hLayout->addWidget(labelsContainer[(*it)]);
        QWidget *hWidget = new QWidget();
        widgets[*it] = hWidget;
        hWidget->setLayout(hLayout);
        layout->addWidget(hWidget);
    }
    widget->setLayout(layout);
    //ui->scrollArea -> setWidget(widget);
    /// add new buttons ---<
}

void FlycapRecorder::buttonClicked(int i)
{
    bool exist = false;
    for(auto it = widgets.begin(); it!=widgets.end();++it)
    {
        if(it.key()==i)
            exist = true;
    }
    if(!exist) return;
    qDebug() << "button" << i << "clicked";
    scene->highlightRect(i);
}

void FlycapRecorder::delButtonClicked(int i)
{
    if(state==PROCESS)
        return;
    bool exist = false;
    for(auto it = widgets.begin(); it!=widgets.end();++it)
    {
        if(it.key()==i)
            exist = true;
    }
    if(!exist) return;
    widgets[i]->hide();
    QLayoutItem *item = widgets[i]->layout()->takeAt(0);
    QLayoutItem *item2 = widgets[i]->layout()->takeAt(1);
    QLayoutItem *item3 = widgets[i]->layout()->takeAt(2);
    delete item;
    delete item2;
    delete item3;
    delete labelsContainer[i];
    labelsContainer.remove(i);
    buttonsContainer.remove(i);
    layout->removeWidget(widgets[i]);
    widget->setLayout(layout);
    //ui->scrollArea -> setWidget(widget);
    scene->deleteByID(i);
    scene->saveAll();
    //QMetaObject::invokeMethod(&processor,"addInitialImage", Qt::BlockingQueuedConnection, Q_ARG(myMap, scene->getImageRects(true)));
}

void FlycapRecorder::btnReconnectClicked()
{
    camera.disconnectCam();
    camera.setDevId(0);
    camera.connectCam();
    qDebug() << "Connected" << camera.isConnected();
    camera.getResolution(&res);
}

void FlycapRecorder::frameProcessed()
{
    //    qDebug() << "dooooone";
    framesProcessing --;
}

void FlycapRecorder::stopClicked()
{
    if(state==PROCESS)
    {
        state = NEUTRAL;
        formatButtons();
//        ui->btnStart->setEnabled(false);
//        ui->btnStop->setEnabled(false);
//        if(isRunning){
//            isRunning = false;
//            QMetaObject::invokeMethod(&writer, "finalize", Qt::QueuedConnection);
//        }
    }
    if(state==PREPARE)
    {
        scene->cancel();
        saveButtons();
        state=NEUTRAL;
        formatButtons();
        setNoRect();
    }

}

void FlycapRecorder::receiveBadRect(qint64 id)
{
    scene->setBadRect(id);
}

void FlycapRecorder::grabframe()
{
    if(resource == CAMERA)
    {
        if(!isRunning) return;
        if(framesProcessing>1000) {
            qDebug() << "skip";
            QTimer::singleShot(1,this,&FlycapRecorder::grabframe);
            return;
        }

        frame = camera.getFrame();
        const qint64 ts = QDateTime::currentMSecsSinceEpoch();

        if(frame == 0){
            QTimer::singleShot(1,this,&FlycapRecorder::grabframe);
            return;
        }
        const cv::Mat img = cv::Mat(res.height(),res.width(),CV_16U,frame,0);
        uchar * imgbgrprt = new uchar[res.height()*res.width()*3];
        if(rgb16.empty()) rgb16 = cv::Mat(res.height(),res.width(),CV_16UC3);
        cv::cvtColor(img,rgb16,CV_BayerBG2RGB);


        uint8_t * dst = imgbgrprt;
        int16_t * src = (int16_t *)(rgb16.data);
        const int pixels =  res.height()* res.width()*3;
        for(int p = 0; p < pixels; p++ ){
            *dst = static_cast<uint8_t>((*src) >> 8);
            dst++;
            src++;
        }

        delete[] frame;
        previewCounter++;

        if(previewCounter > 3 || !isRecording){

            QImage qimage((const uchar *)imgbgrprt,res.width(),res.height(),QImage::Format_RGB888);
            pixmap=QPixmap::fromImage(qimage);
            if(scene->isEmpty()){
                scene->setImage(pixmap);
                scene->setSceneRect(0, 0, pixmap.width(), pixmap.height());
                fitInView();
                //tPixmapItem = scene->addPixmap(pixmap);
            }else{                
                scene->setImage(pixmap);
                if(cameraOnFirstTime)
                {
                    scene->setSceneRect(0, 0, pixmap.width(), pixmap.height());
                }
                //tPixmapItem->setPixmap(pixmap);
            }
            previewCounter = 0;
        }
        if(state==PROCESS)
        {
            //QMetaObject::invokeMethod(&processor,"process", Qt::AutoConnection, Q_ARG(myMap, scene->getImageRects()));
            if(!processor.isActive)
                QMetaObject::invokeMethod(&processor,"process", Qt::QueuedConnection, Q_ARG(DataContainer, scene->getImageRects()));
        }
        if(isRecording){
            framesProcessing++;
            QMetaObject::invokeMethod(&writer, "writeFrame", Qt::QueuedConnection, Q_ARG( uchar *, imgbgrprt));
        }else{
            delete[] imgbgrprt;
        }

        framesInSec ++;
        if(ts-lastSec > 1000){
            qDebug() << "FPS "<< framesInSec << " buffer "<< framesProcessing;
            lastSec = ts;
            framesInSec = 0;
        }

    }
    QTimer::singleShot(1,this,&FlycapRecorder::grabframe);
}

void FlycapRecorder::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);
    fitInView();
}

void FlycapRecorder::fitInView()
{
    ui->preview->setTransform(QTransform());
    qreal scale;
    if((double)ui->preview->width()/ui->preview->height() >
            (double)res.width()/res.height()){
        scale = (double)ui->preview->height()/ res.height();
    }else{
        scale = (double)ui->preview->width()/ res.width();
    }
    scale*=0.9;
    ui->preview->scale(scale,scale);
}

void FlycapRecorder::closeEvent (QCloseEvent *event)
{
    stopClicked();
}

void FlycapRecorder::on_btnSetRect_clicked()
{
    if(state==NEUTRAL)
    {
        QRectF rect;
        rect.setX(10);
        rect.setY(10);
        rect.setWidth(200);
        rect.setHeight(200);
        scene->setCanResize(true);
        //scene->setRect(rect,true);

        state = PREPARE;

        formatButtons();
        return;
    }
    if(state==PREPARE)
    {
        scene->deleteAll();
        return;
    }
}

void FlycapRecorder::formatButtons()
{
    if(state==PREPARE)
    {
        ui->btnStart->setEnabled(true);
        ui->btnStart->setText("Save");
        ui->btnStop->setText("Cancel");
        ui->btnSetRect->setText("Clear");
        ui->btnProcess->setEnabled(false);
    }
    if(state==PROCESS)
    {
        ui->btnStart->setEnabled(false);
        ui->btnStart->setText("Start");
        ui->btnStop->setText("Stop");
        ui->btnProcess->setText("Processing");
        ui->btnSetRect->setText("SetRect");
        ui->btnSetRect->setEnabled(false);
        ui->btnProcess->setEnabled(false);
    }
    if(state==NEUTRAL)
    {
        ui->btnStart->setEnabled(false);
        ui->btnStart->setText("Start");
        ui->btnStop->setText("Finalize");
        ui->btnProcess->setText("Processing");
        ui->btnSetRect->setText("SetRect");
        ui->btnSetRect->setEnabled(true);
        ui->btnProcess->setEnabled(true);
    }
    if(resource==CAMERA)
        ui->btnSetImg->setEnabled(false);
    if(resource==IMAGE)
        ui->btnSetImg->setEnabled(true);
}

void FlycapRecorder::on_btnProcess_clicked()
{
    state = PROCESS;
    formatButtons();
    if(resource == IMAGE)
    {
        QMetaObject::invokeMethod(&processor,"process", Qt::QueuedConnection, Q_ARG(DataContainer, scene->getImageRects()));
    }
}

void FlycapRecorder::on_radioButton_clicked()
{
    qDebug() << "RESOURCE TYPE -> CAMERA";
    resource = CAMERA;
    cameraOnFirstTime = true;
    processor.resource = resource;
    formatButtons();
}

void FlycapRecorder::on_radioButton_2_clicked()
{
    qDebug() << "RESOURCE TYPE -> IMAGE";
    resource = IMAGE;
    processor.resource = resource;
    formatButtons();
}

void FlycapRecorder::on_btnSetImg_clicked()
{
    QFileDialog dialog;

    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter("Images (*.png *.jpg)");

    imgPath = dialog.getOpenFileName();

    if(imgPath!="")
    {
        QPixmap pixmap(imgPath);
        scene->setImage(pixmap);
        scene->setSceneRect(0, 0, pixmap.width(), pixmap.height());
        fitInView();
    }
}
