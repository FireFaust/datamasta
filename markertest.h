#ifndef FLYCAPRECORDER_H
#define FLYCAPRECORDER_H

#include <QMainWindow>
#include <QDateTime>
#include <QTimer>
#include <QThread>
#include <QGraphicsPixmapItem>

#include <QThread>
#include <QList>
#include <QMap>
#include <QPushButton>
#include <QLabel>
#include <QSignalMapper>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGraphicsDropShadowEffect>
#include <QDir>
#include <QFileDialog>


#include "rectselectscene.h"
#include "writer.h"
#include "flycap.h"
#include "colormanager.h"
#include "processor.h"

namespace Ui {
class FlycapRecorder;
}

typedef enum
{
    PREPARE,
    PROCESS,
    NEUTRAL
}ProcessingState;

class FlycapRecorder : public QMainWindow
{
    Q_OBJECT

public:
    explicit FlycapRecorder(QWidget *parent = 0);
    ~FlycapRecorder();
private slots:
    void grabframe();
    void stopClicked();
    void frameProcessed();
    void btnStartClicked();
    void btnReconnectClicked();

    void formatButtons();

    void buttonClicked(int);
    void delButtonClicked(int);

    void on_btnSetRect_clicked();

    void on_btnProcess_clicked();

    void receiveBadRect(qint64);

    void receiveFinalResult(qint64, double dist);
    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_btnSetImg_clicked();

private:
    Ui::FlycapRecorder *ui;

    QString imgPath;
    bool saved;

    cv::Mat userImage;

    FlyCap camera;

    QPixmap pixmap;

    QThread writerThread;
    Writer writer;

    QThread processorThread;
    Processor processor;

    QRect res;

    uchar * frame;
    qint64 lastSec;
    int framesInSec;

    ProcessingState state;
    ImageResource resource;

    bool isRunning;
    bool isRecording;
    bool cameraOnFirstTime;
    int framesProcessing;
    int previewCounter;

    cv::Mat rgb16;

    QGraphicsPixmapItem * tPixmapItem;
    //QGraphicsScene * scene;
    RectSelectScene * scene;

    cv::Mat img_display;

    void fitInView();

    void setNoRect();
    QList<QPushButton*> carList;
    QMap<qint64, QWidget *> widgets;
    QMap<qint64, QLabel *> labelsContainer;
    QMap<qint64, QPushButton *> buttonsContainer;
    QSignalMapper *btnSignalMapper;
    QSignalMapper *delSignalMapper;
    QVBoxLayout* layout;
    QWidget* widget;

    QPalette _normal;
    QPalette _warning;
    QPalette _critical;

    double const border = sqrt(2);

    void saveButtons();

    ColorManager colorManager;
protected:
    void resizeEvent(QResizeEvent* event);
    void closeEvent (QCloseEvent *event);
};

#endif // FLYCAPRECORDER_H
