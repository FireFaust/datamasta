#include "processor.h"
#include <QTime>
#include <QDateTime>

Processor::Processor(QObject *parent) : QObject(parent)
{
    qDebug() << "Processor::Processor";
    infoPrinted = false;
    isActive = false;
    insertInFirst = true;
    for(int i=0; i< QThread::idealThreadCount();i++)
    {
        Worker *worker = new Worker(i);
        QThread *thread = new QThread(this);
        QObject::connect(worker,SIGNAL(sendRect(qint64,qint64,QRectF)),this,SLOT(threadResult(qint64,qint64,QRectF)));
        worker->moveToThread(thread);
        thread->start();
        activeThreads[i] = false;
        workers.push_back(worker);
        threads.push_back(thread);
    }
}

Processor::~Processor()
{

}

void Processor::checkRect(qint64 id, cv::Mat dst)
{
    QMap<qint64,qint64> horizontal;
    QMap<qint64,qint64> vertical;
    int counter = 0;
    /// for the (Horde)Atis --->
    /// vertical lines --->
    for(int Y = 0; Y < dst.rows; Y++)
    {
        for(int x = 0; x < dst.cols; x++)
        {
            counter = 0;
            if(dst.at<uchar>(Y,x)>0)
            {
                int X = x;
                for(int y = 0; y < dst.rows-1; y++)
                {
                    ///go deep
                    if(dst.at<uchar>(y+1,X)>0)
                    {
                        counter++;
                        continue;
                    }
                    else
                    if(X>0)
                    {
                        if(dst.at<uchar>(y,X-1)>0)
                        {
                            if(X>0)
                                X--;
                            counter++;
                            continue;
                        }
                        else
                        {
                            if(dst.at<uchar>(y,X+1)>0)
                            {
                                X++;
                                counter++;
                                continue;
                            }
                            else
                            {
                                if(counter>10)
                                {
                                    vertical[x] = counter;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /// vertical lines <---
    /// horizontal lines --->
    counter = 0;
    for(int X = 0; X < dst.cols; X++)
    {
        for(int y = 0; y < dst.rows; y++)
        {
            counter = 0;
            if(dst.at<uchar>(y,X)>0)
            {
                int Y = y;
                for(int x = 0; x < dst.cols-1; x++)
                {
                    ///go deep
                    if(dst.at<uchar>(Y,x+1)>0)
                    {
                        counter++;
                        continue;
                    }
                    else
                    if(Y>0)
                    {
                        if(dst.at<uchar>(Y-1,x)>0)
                        {
                            if(Y>0)
                                Y--;
                            counter++;
                            continue;
                        }
                        else
                        {
                            if(dst.at<uchar>(Y+1,x)>0)
                            {
                                Y++;
                                counter++;
                                continue;
                            }
                            else
                            {
                                if(counter>10)
                                {
                                    horizontal[y] = counter;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /// horizontal lines <---
    qDebug() <<"vert"<< vertical.size();
    qDebug() <<"hor"<< horizontal.size();
    /// for the (Horde)Atis <---
    /// OpenCV --->
    cv::Mat color_dst;
    cv::cvtColor( dst, color_dst, CV_GRAY2BGR );
    std::vector<cv::Vec4i> lines;
    cv::HoughLinesP( dst, lines, 1, CV_PI/90, 10, 0,0);
    qDebug() <<"cv lines size"<< lines.size() << id;
    int horizontalLines = 0;
    int verticalLines = 0;
    for( size_t i = 0; i < lines.size(); i++ )
    {
        if(abs(lines[i][0]-lines[i][2])<45)
        {
            verticalLines++;
        }
        if(abs(lines[i][1]-lines[i][3])<45)
        {
            horizontalLines++;
        }
        cv::line( color_dst, cv::Point(lines[i][0], lines[i][1]),
            cv::Point(lines[i][2], lines[i][3]), cv::Scalar(0,0,255), 1, 4 );
    }
    ///our
//    if(horizontal.size()<8||vertical.size()<8)
//        emit badRect_HV(id);
    /// cv
    if(horizontalLines<8||verticalLines<8)
        emit badRect_CV(id);
    qDebug() <<"cv rez"<< verticalLines << horizontalLines;
    /// OpenCV <---
//    cv::imshow("image",color_dst);
//    cv::waitKey(100000);
}

void Processor::addInitialImage(myMap container)
{
    clearAll();
    qDebug() << "Processor::addInitialImage";
//    initialImages = container;
//    for(auto it = container.begin();it!=container.end();++it)
//    {
//        cv::Mat rbg4 = qimageToMatCpy((*it).first);
//        cv::cvtColor(rbg4,initialMat[it.key()].first,CV_RGBA2RGB);
//        initialMat[it.key()].second=(*it).second;
//        cv::Mat src = initialMat[it.key()].first;
//        cv::Mat src_bnw;
//        cv::cvtColor(src,src_bnw,CV_RGB2GRAY);
//        cv::Mat dsc;
//        cv::adaptiveThreshold(src_bnw,dsc,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,CV_THRESH_BINARY,15,5);
//        cv::GaussianBlur(dsc,src_bnw,cv::Size(7,7),3,3);
//        cv::threshold(src_bnw,dsc,180,255,CV_THRESH_BINARY);
////        cv::imshow("image",dsc);
////        cv::waitKey(10000);
//        cv::Canny(dsc,src,60,180,3);
//        checkRect(it.key(),src);
////        cv::imshow("image",src);
////        cv::waitKey(10000);
//        cv::GaussianBlur(src,dsc,cv::Size(5,5),3,3);

//        cv::Mat hsv;
//        std::vector<cv::Mat> channels;
//        cv::cvtColor(initialMat[it.key()].first,hsv,CV_RGB2YCrCb);
//        cv::split(hsv,channels); //split the image into channels
//        cv::equalizeHist(channels[0], channels[0]); //equalize histogram on the 1st channel (Y)
//        cv::merge(channels,hsv); //merge 3 channels including the modified 1st channel into one image

//        cv::cvtColor(hsv,hsv,CV_YCrCb2RGB);

//        cv::Mat gray;
//        cv::cvtColor(hsv,gray,CV_RGB2GRAY);

//        cv::threshold(gray,initialMat[it.key()].first,50,250,CV_THRESH_BINARY);

//        initialEdge[it.key()] = dsc;
//        if(blackImage.empty())
//        {
//            blackImage = cv::Mat(dsc.rows,dsc.cols,CV_8UC1,cv::Scalar(0));
//        }
////        if(tGrid.empty()){
////            tGrid = cv::Mat(cv::Size(initialMat[it.key()].first.size().width*22,
////                            initialMat[it.key()].first.size().height*22), CV_8UC1);
////        }
////        cv::imshow("image",src);
////        cv::waitKey(10000);
////        cv::imshow("image",dsc);
////        cv::waitKey(10000);
//    }
////    for(int i=0; i< QThread::idealThreadCount();i++)
////    {
////        QMetaObject::invokeMethod(workers[i],"start", Qt::QueuedConnection);
////    }
//    infoPrinted = false;
}

void Processor::threadResult(qint64 id,qint64 th_id, QRectF rect)
{
    activeThreads[th_id]  = false;
    processedRects[id] = true;
    emit sendRect(id,rect);
    //startWork();
}

void Processor::process(myMap container)
{

}

void Processor::process(DataContainer container)
{
    QDir buildDir;
    cv::Mat mainImage = qimageToMatCpy(container.first);
    if(resource==CAMERA)
    {
        QDir dir(buildDir.absolutePath()+"/camera");
        this->dir.setPath(dir.absolutePath());
    }
    else if(resource == IMAGE)
    {
        QDir dir(buildDir.absolutePath()+"/image");
        this->dir.setPath(dir.absolutePath());
    }
    if(!dir.exists())
        dir.mkpath(".");
    QDir cDir(dir.absolutePath() +"/" + QDateTime::currentDateTime().toString("dd.MM.yyyy"));
    if(!cDir.exists())
        cDir.mkpath(".");
    for (auto it = container.second.begin(); it != container.second.end();++it)
    {
        QString name;
        name += QString::number(it.key());
        name += "_";
        name += QString::number(QDateTime::currentMSecsSinceEpoch());
        name += ".jpg";
        cv::Rect rect;
        rect.x = (int)(*it).x();
        rect.y = (int)(*it).y();
        rect.width = (int)(*it).width();
        rect.height = (int)(*it).height();
        cv::Mat img = mainImage(rect);
        cv::imwrite(QString(cDir.absolutePath()+"/"+name).toStdString(),img);
    }
}

void Processor::clearAll()
{
    qDebug() << "Processor::clearAll";
    initialImages.clear();
    initialMat.clear();
    initialEdge.clear();
    blackImage.release();
    processedRects.clear();
    activeRects.clear();
    data.clear();
    for(int i=0; i< QThread::idealThreadCount();i++)
    {
        activeThreads[i] = false;
    }
    infoPrinted = false;
}

void Processor::printInfo()
{
    qDebug() << "Processor::printInfo";
    qDebug() << "initial images size" << initialImages.size();
}

cv::Mat Processor::qimageToMatCpy( const QImage &inImage, bool inCloneImageData)
{
    switch ( inImage.format() )
    {
     // 8-bit, 4 channel
     case QImage::Format_RGB32:
     {
        cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

        return (inCloneImageData ? mat.clone() : mat);
     }

     // 8-bit, 3 channel
     case QImage::Format_RGB888:
     {
        if ( !inCloneImageData )
           qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";

        QImage   swapped = inImage.rgbSwapped();

        return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
     }

     // 8-bit, 1 channel
     case QImage::Format_Indexed8:
     {
        cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

        return (inCloneImageData ? mat.clone() : mat);
     }

     default:
        qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
        break;
    }

    return cv::Mat();
}
