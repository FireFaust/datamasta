#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.h>

#include <QDebug>
#include <QMap>
#include <QImage>
#include <QDir>

#include <QThread>

#include "worker.h"

typedef QMap<qint64,QPair<QImage,QRectF> > myMap;
typedef QPair<QImage,QMap<qint64,QRectF> > DataContainer;

typedef enum
{
    CAMERA,
    IMAGE
}ImageResource;

QDebug inline operator<<(QDebug d, const  BestMatch &f){
        QDebug nsp = d.nospace();
        nsp << "Best Match{";
        nsp << "id[" << f.id <<"]";
        nsp << "result[" << f.result << "]";
        nsp << "rect[" << f.rect.x << " " << f.rect.y <<"]}";
        return d;
}

class Processor : public QObject
{
    Q_OBJECT
public:
    explicit Processor(QObject *parent = 0);
    bool isActive;
    ImageResource resource;
    ~Processor();
private:
    QMap<qint64,QPair<QImage,QRectF> >  initialImages;
    QMap<qint64,QPair<cv::Mat,QRectF> >  initialMat;
    QMap<qint64,cv::Mat >  initialEdge;
    void clearAll();
    bool infoPrinted;
    void printInfo();
    cv::Mat qimageToMatCpy( const QImage &inImage, bool inCloneImageData = true );
    double countCanny(cv::Mat image);
    double countDiff(cv::Mat src1, cv::Mat src2, cv::Mat dsc, qint64 id, int speed,cv::Mat debug = cv::Mat());
    cv::Rect prepareRect(cv::Rect rect, int iWidth, int iHeight, int speed);
    void checkRect(qint64,cv::Mat);
    cv::Mat blackImage;
    BestMatch bestResult[2];
    bool insertInFirst;
    QList<Worker *> workers;
    QList<QThread *> threads;
    QMap<qint64,bool> activeThreads;
    QMap<qint64,bool> activeRects;
    QMap<qint64,bool> processedRects;
    QMap<qint64,ImageData> data;
    void startWork();
    QDir dir;
signals:
    void sendRect(qint64,QRectF);
    void badRect_HV(qint64); /// using own functions
    void badRect_CV(qint64); /// using opwncv functions
public slots:
    void addInitialImage(myMap);
    void process(myMap);
    void process(DataContainer);
    void threadResult(qint64, qint64 th_id, QRectF);
private:
    cv::Mat tGrid;
};

#endif // PROCESSOR_H
