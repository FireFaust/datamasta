#include "rectselectscene.h"
#include <cmath>
#include <math.h>

RectSelectScene::RectSelectScene(QObject *parent):
    QGraphicsScene(parent)
{
    imageItem = 0;
    counter = 0;
    mode = Idle;
    empty = true;

    background = new QGraphicsItemGroup();

    this->addItem(background);
    canResize = false;

    rectRegion = new QGraphicsRectItem(0,0,0,0);
    rectRegion->setBrush(QColor(255, 0, 0, 56));
    rectRegion->setPen(QColor(255, 0, 0, 0));

    rectResize = new QGraphicsRectItem(94.5,194.5,5,5,rectRegion);
    rectResize->setBrush(QColor(0, 255, 0, 56));
    rectResize->setPen(QPen(QColor(255,0,0,56),1,Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));

    //rectResize->setFlag(QGraphicsItem::ItemIsMovable,true);

    lastHighlight = -1;
    isHighlighted = false;

    this->addItem(rectRegion);
}

void RectSelectScene::setImage(QByteArray image)
{
    if(image.isEmpty()) return;
    if(imageItem != 0){
        this->removeItem(imageItem);
        delete imageItem;
    }
    QImage img = QImage::fromData(image,"jpg");
    imageItem = new QGraphicsPixmapItem(QPixmap::fromImage(img),background);
}

void RectSelectScene::setImage(QPixmap image)
{
    if(image.isNull()) return;
    if(imageItem != 0){
        this->removeItem(imageItem);
        delete imageItem;
    }
    empty = false;
    QImage img = image.toImage();
    imageItem = new QGraphicsPixmapItem(QPixmap::fromImage(img),background);
}

//QMap<qint64,QPair<QImage,QRectF> > RectSelectScene::getImageRects(bool staticData)
//{
//    QMap<qint64,QPair<QImage,QRectF> > container;
//    QImage image;
//    QImage mainImage = imageItem->pixmap().toImage();
//    for(auto it = rectangles.begin();it!=rectangles.end();++it)
//    {
//        QPair<QImage,QRectF> pair;
//        QRectF rect = (*it)->rect().toRect();
//        if(!staticData)
//        {
//            if(rect.x()-11>mainImage.rect().x())
//                rect.setX(rect.x()-11);
//            if(rect.y()-11>mainImage.rect().y())
//                rect.setY(rect.y()-11);
//            if(rect.width()+11<mainImage.rect().width())
//                rect.setWidth(rect.width()+11);
//            if(rect.height()+11<mainImage.rect().height())
//                rect.setHeight(rect.height()+11);
//        }
//        image = mainImage.copy(rect.toRect());
//        pair.first = image; pair.second = rect;
//        container[it.key()] = pair;
//    }
//    return container;
//}

QPair<QImage,QMap<qint64,QRectF> > RectSelectScene::getImageRects()
{
    QPair<QImage,QMap<qint64,QRectF> > container;
    QImage mainImage = imageItem->pixmap().toImage();
    container.first = mainImage;
    for(auto it = rectangles.begin();it!=rectangles.end();++it)
    {
        QRectF rect = (*it)->rect().toRect();
        container.second[it.key()] = rect;
    }
    return container;
}

double RectSelectScene::distance(double dX0, double dY0, double dX1, double dY1)
{
    return sqrt((dX1 - dX0)*(dX1 - dX0) + (dY1 - dY0)*(dY1 - dY0));
}

void RectSelectScene::receiveRect(qint64 id, QRectF rect)
{
    //qDebug() <<"RectSelectScene::receiveRect";
    bool canProcess = false;
    for(auto it = rectangles.begin(); it!=rectangles.end();++it)
    {
        if(it.key()==id)
        {
            canProcess = true;
            break;
        }
    }
    if(!canProcess) return;
    qDebug() << "id["<< id<<"]" << "x["<<rect.x()-rectangles[id]->rect().x()<<"]" << "y["<<rect.y()-rectangles[id]->rect().y() <<"]";
    QColor color = rectangles[id]->brush().color();

    for(auto it = movementRect.begin();it!=movementRect.end();++it)
    {
        if(it.key()==id)
        {
            delete lineItems[it.key()];
            delete movementRect[it.key()];
            movementRect.remove(it.key());
            lineItems.remove(it.key());
            break;
        }
    }
    if(rect==rectangles[id]->rect())
    {
        emit carMoved(id,0);
        return;
    }
    double dist = distance(rectangles[id]->rect().x()+rectangles[id]->rect().width()/2.,
                           rectangles[id]->rect().y()+rectangles[id]->rect().height()/2.,
                           rect.x()+rect.width()/2.,
                           rect.y()+rect.height()/2.);
    if( /*(abs(rect.x()-rectangles[id]->rect().x())>=10) || (abs(rect.y()-rectangles[id]->rect().y())>=10)*/dist>11)
    {
        emit carMoved(id,12);
        return;
    }
    color.setAlpha(150);
    QGraphicsRectItem *rectF = new QGraphicsRectItem();
    rectF->setBrush(color);
    rectF->setPen(QColor(255, 0, 0, 0));
    rectF->setRect(rect);
    QGraphicsLineItem *lineItem = new QGraphicsLineItem();
    lineItem->setPen(QPen((QColor(255, 0, 0, 255)),3,Qt::SolidLine));

    movementRect[id] = rectF;
    qreal x1,x2,y1,y2;
    x1 = rectangles[id]->rect().x()+rectangles[id]->rect().width()/2.;
    x2 = movementRect[id]->rect().x()+movementRect[id]->rect().width()/2.;
    y1 = rectangles[id]->rect().y()+rectangles[id]->rect().height()/2.;
    y2 = movementRect[id]->rect().y()+movementRect[id]->rect().height()/2.;
    if(x2>x1) x2+=abs(x2-x1);
    else if(x2<x1) abs(x2-x1);
    if(y2>y1) y2+=abs(y2-y1);
    else if(y2<y1)y2-=abs(y2-y1);
    lineItem->setLine(x1,y1,x2,y2);

    lineItems[id] = lineItem;
    this->addItem(movementRect[id]);
    this->addItem(lineItems[id]);
    emit carMoved(id,dist);
}

void RectSelectScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0|| !canResize) return;
    if (mouseEvent->button() == Qt::LeftButton)
    {
        mouseButton = MouseButton::Left;
    }
    else
    {
        if (mouseEvent->button() == Qt::RightButton)
        {
            mouseButton = MouseButton::Right;
        }
        else return;
    }
    if(mouseButton==MouseButton::Left)
    {
        lastPos = mouseEvent->scenePos();
        startRect = QRectF(lastPos,QSize(0,0));
        mode = ResizeRect;
    }
    else
    {
        if(mouseButton==MouseButton::Right)
        {
            rectRegion->setRect(QRectF(0,0,0,0));
            mode = Idle;
            QPointF pos = mouseEvent->scenePos();
            deleteByPoint(pos);
        }
        else
        {
            QGraphicsScene::mouseMoveEvent(mouseEvent);
        }
    }
}

void RectSelectScene::deleteByID(qint64 id)
{
    delete rectangles[id];
    //delete tempRectangles[id];
    emit rectangleDeleted(id);
    rectangles.remove(id);
    //tempRectangles.remove(id);
    for(auto iit = ids.begin(); iit<ids.end();++iit)
    {
        if((*iit)==id)
            ids.removeAt(iit-ids.begin());
    }
    for(auto iit = movementRect.begin(); iit!=movementRect.end();++iit)
    {
        if(id==iit.key())
        {
            delete (*iit);
            delete lineItems[id];
            movementRect.remove(id);
            lineItems.remove(id);
            break;
        }
    }
    tempIds = ids;
}

void RectSelectScene::deleteByPoint(QPointF pos)
{
    for (auto it = rectangles.end()-1; it!=rectangles.begin()-1;--it)
    {
        if((*it)->rect().contains(pos))
        {
            delete (*it);
            emit rectangleDeleted(it.key());
            qDebug() << it.key();
            for(auto iit = movementRect.begin(); iit!=movementRect.end();++iit)
            {
                if(it.key()==iit.key())
                {
                    delete (*iit);
                    delete lineItems[it.key()];
                    movementRect.remove(it.key());
                    lineItems.remove(it.key());
                    break;
                }
            }
            for(auto iit = ids.begin(); iit<ids.end();++iit)
            {
                if((*iit)==it.key())
                    ids.removeAt(iit-ids.begin());
            }
            rectangles.remove(it.key());
            break;
        }
    }
}

void RectSelectScene::setBadRect(qint64 id)
{
    badRectSavedColor[id] = rectangles[id]->brush().color();
    rectangles[id]->setBrush(QColor(255, 0, 0, 120));
}

void RectSelectScene::highlightRect(qint64 id)
{
    if(lastHighlight == id && isHighlighted)
    {
        rectangles[lastHighlight]->setPen(QColor(255, 0, 0, 0));
        rectangles[lastHighlight]->setBrush(lastColor);
        isHighlighted = false;
        return;
    }
    if(lastHighlight==-1)
    {
        lastHighlight = id;
        lastColor = rectangles[id]->brush().color();
    }
    else
    {
        bool exist = false;
        for(auto it = rectangles.begin(); it!=rectangles.end();++it)
        {
            if(it.key()==lastHighlight)
            {
                exist = true;
                break;
            }
        }
        if(exist)
        {
            rectangles[lastHighlight]->setPen(QColor(255, 0, 0, 0));
            rectangles[lastHighlight]->setBrush(lastColor);
        }
        lastHighlight = id;
        lastColor = rectangles[id]->brush().color();
    }
    rectangles[id]->setPen(QColor(0, 0, 0, 255));
    rectangles[id]->setBrush((QColor(255,255,255,100)));
    isHighlighted = true;
}

void RectSelectScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0 || !canResize) return;
    if( mouseButton==MouseButton::Left&&mode == ResizeRect)
    {
        resizeRect(mouseEvent->scenePos());
    }
    else
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void RectSelectScene::setNewRect(QColor color)
{
    QGraphicsRectItem *rectF = new QGraphicsRectItem();
    rectF->setBrush(color);
    rectF->setPen(QColor(255, 0, 0, 0));
    rectF->setRect(rectRegion->rect());
    rectangles[counter] = rectF;
    counter++;
    rectRegion->setRect(QRectF(0,0,0,0));
    QList<QGraphicsItem*> item = this->items();
    for(auto it = rectangles.begin();it!= rectangles.end();++it)
    {
        bool insert = true;
        for(auto iit = item.begin(); iit<item.end();iit++)
        {
            if((*iit)==(*it))
            {
                insert = false;
                break;
            }
        }
        if(insert)
        {
            this->addItem((*it));
            ids.push_back(it.key());
        }
    }
}

void RectSelectScene::deleteAll()
{
    for (auto it = rectangles.begin(); it!=rectangles.end();++it)
    {
        delete (*it);
        emit rectangleDeleted(it.key());
    }
    for (auto it = tempRectangles.begin(); it!=tempRectangles.end();++it)
    {
        delete (*it);
    }
    for(auto it = movementRect.begin();it!=movementRect.end();++it)
    {
        delete lineItems[it.key()];
        delete movementRect[it.key()];
        movementRect.remove(it.key());
        lineItems.remove(it.key());
    }
    badRectSavedColor.clear();
    ids.clear();
    tempIds.clear();
    rectangles.clear();
    tempRectangles.clear();
    movementRect.clear();
    lineItems.clear();
}

void RectSelectScene::cancel()
{
    for (auto it = tempRectangles.begin(); it!=tempRectangles.end();++it)
    {
        bool exists = false;
        for (auto iit = rectangles.begin(); iit!=rectangles.end();++iit)
        {
            if(iit.key()==it.key())
            {
                exists = true;
                break;
            }
        }
        if(!exists)
        {
            rectangles[it.key()] = (*it);
            this->addItem(rectangles[it.key()]);
            QPair<qint64,QColor> pair;
            bool badColor = false;
            for (auto b_it = badRectSavedColor.begin(); b_it != badRectSavedColor.end();++b_it)
            {
                if(b_it.key()==it.key())
                {
                    badColor = true;
                    pair.first = b_it.key(); pair.second = (*b_it);
                    break;
                }
            }
            if(!badColor)
            {
                pair.first = (qint64)it.key(); pair.second = (*it)->brush().color();
            }
            emit colorReturned(pair);
        }
    }
    QList<qint64> toDelete;
    for (auto it = rectangles.begin(); it!=rectangles.end();++it)
    {
        bool exists = false;
        for (auto iit = tempRectangles.begin(); iit!=tempRectangles.end();++iit)
        {
            if(iit.key()==it.key())
            {
                exists = true;
                break;
            }
        }
        if(!exists)
        {
            toDelete.push_back(it.key());
        }
    }
    for (auto it = toDelete.begin(); it!=toDelete.end();++it)
    {
        delete rectangles[(*it)];
        emit rectangleDeleted((*it));
        rectangles.remove((*it));
    }
    ids = tempIds;
}

void RectSelectScene::saveAll()
{
    badRectSavedColor.clear();
    for(auto it = tempRectangles.begin(); it != tempRectangles.end(); ++it)
    {
        delete (*it);
    }
    tempRectangles.clear();
    for (auto it = rectangles.begin(); it!=rectangles.end();++it)
    {
        QGraphicsRectItem *rectF = new QGraphicsRectItem();
        rectF->setBrush((*it)->brush().color());
        rectF->setPen(QColor(255, 0, 0, 0));
        rectF->setRect((*it)->rect());
        tempRectangles[it.key()] = rectF;
    }
    tempIds = ids;
}

void RectSelectScene::update()
{
    for(auto it = rectangles.begin();it!= rectangles.end();++it)
    {
        this->addItem((*it));
    }
}

void RectSelectScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0 || !canResize) return;
    rectRegion->setRect(QRectF(0,0,0,0));
    if( mode == ResizeRect && mouseButton==MouseButton::Left)
    {
        resizeRect(mouseEvent->scenePos());
        emit rectangleReady(counter);
        mode = Idle;
    }
    else
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void RectSelectScene::resizeRect(QPointF mouse)
{
    QPointF diff = mouse - lastPos;
    QRectF rectRegionR = getRect();
    rectRegionR.setX(round(startRect.x()));
    rectRegionR.setY(round(startRect.y()));
    rectRegionR.setWidth(round(startRect.width() + diff.x()));
    rectRegionR.setHeight(round(startRect.height() + diff.y()));
    setRect(rectRegionR);
}

void RectSelectScene::moveRect(QPointF mouse)
{
    QPointF diff = mouse - lastPos;
    QRectF rectRegionR = getRect();
    rectRegionR.setX(round(startRect.x() + diff.x()));
    rectRegionR.setY(round(startRect.y() + diff.y()));
    rectRegionR.setWidth(startRect.width());
    rectRegionR.setHeight(startRect.height());
    setRect(rectRegionR);
}

void RectSelectScene::setRect(QRectF rect, bool force)
{
    if(!force){
        if(imageItem == 0) return;
        if(rect.x()+rect.width() > imageItem->boundingRect().width()){
            if(mode == MoveRect){
                int w = rect.width();
                rect.setX(imageItem->boundingRect().width()-rect.width());
                rect.setWidth(w);
            }else{
                rect.setWidth(imageItem->boundingRect().width()-rect.x());
            }
        }

        if(rect.y()+rect.height() > imageItem->boundingRect().height()){
            if(mode == MoveRect){
                int h = rect.height();
                rect.setY(imageItem->boundingRect().height()-rect.height());
                rect.setHeight(h);
            }else{
                rect.setHeight(imageItem->boundingRect().height()-rect.y());
            }
        }

        if(rect.x() < 0){
            int w = rect.width();
            rect.setX(0);
            rect.setWidth(w);
        }
        if(rect.y() < 0){
            int h = rect.height();
            rect.setY(0);
            rect.setHeight(h);
        }

        if(rect.width() < 8) rect.setWidth(8);
        if(rect.height() < 8) rect.setHeight(8);
    }
    rectRegion->setRect(rect);
    /// little gray rect
    //QRectF rectResizeR = QRectF(rect.x()+rect.width()-5.5,rect.y()+rect.height()-5.5,5,5);
    /// my gray rect
//    QRectF rectResizeR = QRectF(rect.x()+rect.width()-15.5,rect.y()+rect.height()-15.5,15,15);
//    rectResize->setRect(rectResizeR);
}
