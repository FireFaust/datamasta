#ifndef RECTSELECTSCENE_H
#define RECTSELECTSCENE_H

#include "math.h"

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QDebug>

enum MouseButton
{
    Left,
    Right
};

class RectSelectScene : public QGraphicsScene
{
    Q_OBJECT
public:

    enum Mode { Idle, MoveRect, ResizeRect };

    RectSelectScene(QObject *parent = 0);

    void setImage(QByteArray image);
    void setImage(QPixmap image);

    void setRect(QRectF rect, bool force = false);
    inline QRectF getRect(){return rectRegion->rect();}
    inline QPointF getPicRect(){if(imageItem == 0) return QPointF(0,0); return QPointF(imageItem->boundingRect().width(),imageItem->boundingRect().height());}
    inline void setCanResize(bool canResize){this->canResize = canResize;}
    void deleteAll();
    inline bool isEmpty(){return empty;}
    inline QList<qint64> getIds(){return ids;}
    void deleteByID(qint64);
    void highlightRect(qint64);
    void setBadRect(qint64);
    void saveAll();
    void cancel();
    //QMap<qint64,QPair<QImage,QRectF> > getImageRects(bool staticData = false);
    QPair<QImage,QMap<qint64,QRectF> > getImageRects();
    double distance(double dX0, double dY0, double dX1, double dY1);
public slots:
    void setNewRect(QColor);
    void receiveRect(qint64,QRectF);
signals:
    void itemSelected(QGraphicsItem *item);
    void rectangleReady(qint64 id);
    void rectangleDeleted(qint64);
    void colorReturned(QPair<qint64,QColor>);
    void carMoved(qint64,double);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    //bool isItemChange(int type);

    Mode mode;
    QGraphicsPixmapItem * imageItem;
    QGraphicsRectItem * rectRegion;
    QGraphicsRectItem * rectResize;
    QGraphicsItemGroup * background;

    QPointF lastPos;
    QRectF startRect;

    MouseButton mouseButton;

    void resizeRect(QPointF mouse);
    void moveRect(QPointF mouse);
    bool canResize;
    bool empty;

    qint64 counter;

    QMap <qint64, QGraphicsRectItem*> rectangles;
    QMap <qint64, QGraphicsRectItem*> tempRectangles;
    QMap <qint64, QGraphicsRectItem*> movementRect;
    QMap <qint64, QColor> badRectSavedColor;
    QMap <qint64, QGraphicsLineItem*> lineItems;
    QList<qint64> ids;
    QList<qint64> tempIds;
    QList<qint64> colorInUse;

    qint64 lastHighlight;
    QColor lastColor;
    bool isHighlighted;

    void deleteByPoint(QPointF);
    void update();
};

#endif // RECTSELECTSCENE_H
