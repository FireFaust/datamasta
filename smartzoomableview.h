#ifndef SMARTZOOMABLEVIEW_H
#define SMARTZOOMABLEVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

class SmartZoomableView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit SmartZoomableView(QWidget *parent = 0);

signals:

public slots:

protected:
    virtual void wheelEvent(QWheelEvent* event);

};

#endif // SMARTZOOMABLEVIEW_H
