#include "worker.h"

#include <QDir>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>

#include <sys/stat.h>

Worker::Worker(QObject *parent) : QObject(parent)
{

}

Worker::Worker(qint64 id)
{
    _id = id;
    _isWorking = false;
}

Worker::~Worker()
{

}

void Worker::setData(ImageData data)
{
    _rectId       = data.rectId;
    _originalMat   = data.originalImage;
    _cannyImage    = data.cannyImage;
    _receivedImage = data.receivedImage;
    _originalRect = data.originalRect;
    _receivedRect = data.receivedRect;
    process();
}

void Worker::start()
{
    process();
}

void Worker::process()
{
    _isWorking = true;
//    BestMatch match;
//    match.id = -1; match.result = INT_MAX;
//    bestResult[0] = match; bestResult[1] = match;
//    timeStart = QDateTime::currentMSecsSinceEpoch();
//    //    for(auto it = containerMat.begin();it!=containerMat.end();++it)
//    //    {
//    int difX      = _receivedRect.x() - _originalRect.x();
//    int difY      = _receivedRect.y() - _originalRect.y();
//    int difWidth  = _receivedRect.width()  - _originalRect.width();
//    int difHeight = _receivedRect.height() - _originalRect.height();
//    cv::Rect rect;
//    rect.x = abs(difX);
//    rect.y = abs(difY);
//    rect.width  = _receivedRect.width()-abs(difWidth);
//    rect.height = _receivedRect.height()-abs(difHeight);
//    //        //qDebug() <<rect.x <<rect.y << rect.height <<rect.width <<(*it).first.cols;
//    cv::Mat mat = _receivedImage(rect);

////    QDir dir;//("smart_storage/resident_capture");
////    QString path = dir.absolutePath();
//////    if(!dir.exists())
//////        dir.mkpath(".");
//////    chmod(dir.absolutePath().toStdString().c_str(),S_IRUSR | S_IWUSR);
////    QString f1 = path;
////    f1.push_back("/ddd");
////    mkdir(f1.toStdString().c_str(),ACCESSPERMS);
////    QFileInfo fi(dir.absolutePath());

////    if(fi.permission(QFile::WriteUser))
////        qDebug() << "25 lighters";

////    QFile file(dir.absolutePath());

////    if(!file.setPermissions(QFile::ReadOwner | QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::WriteOwner | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther))
////    {
////        qWarning() << "unable to set permissions for" << dir.absolutePath();
////    }

////    qDebug() << dir.absolutePath();

//    cv::Mat dif = cv::Mat(mat.rows,mat.cols,CV_8UC1);
//    double result = countDiff(mat,_originalMat,dif,_rectId,1);
//    double idResult = INT32_MAX;
//    cv::Rect idRect = rect;
//    if(result > 0)
//    {
//        cv::Rect rect2 = prepareRect(rect,_receivedRect.width(),_receivedRect.height(),10);
//    //            //qDebug() << rect2.x << rect2.y << rect2.width << rect2.height << (*it).second.width();
//        cv::Mat mat = _receivedImage(rect2);
//        int counter = mat.rows-_originalMat.rows;
//        for(int x = mat.cols-_originalMat.cols; x>=0; x-=1)
//        {
//            for(int y =counter;  y>=0; y-=1)
//            {
//                rect2.x = x;
//                rect2.y = y;
//                rect2.width = rect.width;
//                rect2.height = rect.height;

//                //cv::Mat trez;
//                //trez = tGrid(cv::Rect(rect2.width*x,rect2.height*y,rect2.width,rect2.height));
//                mat = _receivedImage(rect2);
//                //double val = countDiff(mat,initialMat[it.key()].first,dif,src,it.key(),1,trez);
//                double val = countDiff(mat,_originalMat,dif,_rectId,1);
//                if(val<idResult)
//                {
//                    idResult = val;
//                    idRect = rect2;
//                    if(val<bestResult[0].result)
//                    {
//                        bestResult[1] = bestResult[0];
//                        bestResult[0].id = _rectId;
//                        bestResult[0].result = val;
//                        bestResult[0].rect = rect2;
//                    }
//                    else
//                    {
//                        if(val<bestResult[1].result)
//                        {
//                            bestResult[1].id = _rectId;
//                            bestResult[1].result = val;
//                            bestResult[1].rect = rect2;
//                        }
//                     }
//                 }
//            }
//        }
//        double idX, idY;
//        idX = bestResult[0].rect.x;
//        idY = bestResult[0].rect.y;
//        QRectF detectedPos = _originalRect;
//        detectedPos.setX(_receivedRect.x() + idX);
//        detectedPos.setY(_receivedRect.y() + idY);
//        detectedPos.setWidth(_originalRect.width());
//        detectedPos.setHeight(_originalRect.height());
//        _isWorking = false;
//        emit sendRect(_rectId,_id,detectedPos);
//    }
//    else
//    {
        _isWorking = false;
//        emit sendRect(_rectId,_id,_originalRect);
//    }
////    for(;;)
////    {
////        qDebug() << "thread" << _id;
////    }
}

double Worker::countDiff(cv::Mat src1, cv::Mat src2, cv::Mat dsc, qint64 id, int speed,cv::Mat debug)
{
    bool hasD = debug.empty();
    dsc = cv::Mat(src2.rows,src2.cols,CV_8UC1,cv::Scalar(0));
    for(int y = 0; y < src1.size().height; y+=speed)
    {
        for(int x = 0; x < src1.size().width; x+=speed)
        {
//          if(!hasD)
//          {
//              debug.at<uchar>(y,x) = uchar(std::max(0.f,std::min(255.f,cc)));
//          }
          if(_cannyImage.at<uchar>(y,x)>0)
            dsc.at<uchar>(y,x) = abs(src1.at<uchar>(y,x)-src2.at<uchar>(y,x));
        }
    }
    long long counter = 0;
    for(int y = 0; y < src1.size().height; y+=speed)
    {
        for(int x = 0; x < src1.size().width; x+=speed)
        {
            counter += dsc.at<uchar>(y,x);
        }
    }
//    cv::imshow("image",src1);
//    cv::imshow("image2",src2);
//    cv::imshow("image2",dsc);
//    cv::waitKey(1);
    //qDebug() << "White Pixels " <<counter << "initital Pixels" << dsc.cols*dsc.rows;
    return counter;
}

cv::Rect Worker::prepareRect(cv::Rect rect, int iWidth, int iHeight, int speed)
{
    if(speed > 10)
        speed = 10;
    int x,y,width,height;

    if(rect.x>speed)
        x = rect.x-speed;
    else
        x = rect.x;

    if(rect.y>speed)
        y = rect.y-speed;
    else
        y = rect.y;

    if(rect.width<iWidth-2*(speed))
        width = rect.width+2*speed;
    else
        width = rect.width;

    if(rect.height<iHeight-2*(speed))
        height = rect.height+2*speed;
    else
        height = rect.height;
    if(y + height>iHeight)
        height=iHeight-y;
    if(height<0) height = 0;
    if(x + width>iWidth)
        width=iWidth-x;
    if(width<0) width = 0;
    return cv::Rect(x,y,width,height);
}
