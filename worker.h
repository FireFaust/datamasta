#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QRectF>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.h>

#include <QDebug>
#include <QMap>
#include <QImage>
#include <QDateTime>

typedef struct
{
    qint64 rectId;
    cv::Mat originalImage;
    cv::Mat cannyImage;
    cv::Mat receivedImage;
    QRectF originalRect;
    QRectF receivedRect;
} ImageData;

typedef struct
{
    qint64 id;
    QRectF rect;
    qint64 time;
}ResultData;

class BestMatch
{
public:
    qint64 id;
    double result;
    cv::Rect rect;
    friend QDebug operator<< (QDebug d, const BestMatch &model);
};

class Worker : public QObject
{
    Q_OBJECT
public:
    explicit Worker(QObject *parent = 0);
    Worker(qint64 id);
    inline qint64 getId(){return _id;}
    inline bool isWorking(){return _isWorking;}
    ~Worker();
private:
    qint64 _id;
    qint64 _rectId;
    cv::Mat _originalMat;
    cv::Mat _cannyImage;
    cv::Mat _receivedImage;
    QRectF _originalRect;
    QRectF _receivedRect;
    BestMatch bestResult[2];
    bool _isWorking;
    void process();
    double countDiff(cv::Mat src1, cv::Mat src2, cv::Mat dsc, qint64 id, int speed,cv::Mat debug = cv::Mat());
    cv::Rect prepareRect(cv::Rect rect, int iWidth, int iHeight, int speed);
    qint64 timeStart;
signals:
    void complited(qint64,QRectF);
    void sendRect(qint64,qint64,QRectF);
public slots:
    void setData(ImageData);
    void start();
};

#endif // WORKER_H
