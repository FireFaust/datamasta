
#include "writer.h"

#include <float.h>
#include <QDebug>
#include <QDir>
#include <QThread>
#include <QTimer>
#include <QCoreApplication>

#include <sys/resource.h>
#include <unistd.h>

#include <opencv2/highgui/highgui.hpp>

Writer::Writer(QObject *parent):
    QObject(parent)
{
    resizedVideoFrame = nullptr;
    scaleCtx = nullptr;
    outputVideoCtx = nullptr;
    outputVideoFmtCtx = nullptr;
    outputVideoFmt = nullptr;
    outputVideoStream = nullptr;
    outputVideoCodec = nullptr;
    outputPts = 0;
    sourceWidth = 0;
    sourceHeight = 0;
    tFrame = nullptr;
    picture = nullptr;

    av_register_all();
    avcodec_register_all();
    avdevice_register_all();
    avformat_network_init();
    av_log_set_level( AV_LOG_VERBOSE);
}


void Writer::setup(int width, int height, QString filename)
{
    this->filename = filename;
    sourceWidth = width;
    sourceHeight = height;
    qDebug() << sourceWidth<<"x"<<sourceHeight;

    initOutputVideoFile();

    outFrame = av_frame_alloc();
    outFrame->width = sourceWidth;
    outFrame->height = sourceHeight;
    outFrame->format = outputVideoCtx->pix_fmt;
    if(av_image_alloc(outFrame->data, outFrame->linesize, outFrame->width, outFrame->height, outputVideoCtx->pix_fmt, 1) <= 0){
        av_frame_free(&outFrame);
        outFrame = nullptr;
    }

}

void Writer::writeFrame(uchar * frame, qint64 ts)
{

    if(tFrame == nullptr){
        tFrame  = av_frame_alloc();
        if(tFrame != nullptr){
            tFrame->width = sourceWidth;
            tFrame->height = sourceHeight;
            tFrame->format = AV_PIX_FMT_RGB24;
            if(av_image_alloc(tFrame->data, tFrame->linesize, tFrame->width, tFrame->height, AV_PIX_FMT_RGB24, 1) <= 0){
                av_frame_free(&tFrame);
                tFrame = nullptr; // \todo memory leak possible
            }
        }
    }

    if(tFrame == nullptr){
        qDebug() << "faill aloc tFrame";
    }

    memcpy(tFrame->data[0],frame,tFrame->width*tFrame->height*3);

    AVFrame * outputFrame;
    //todo inpi
    if(scaleCtx != nullptr) {
        if(resizedVideoFrame == nullptr){
            resizedVideoFrame = av_frame_alloc();
            if(resizedVideoFrame != nullptr){
                resizedVideoFrame->width = sourceWidth;
                resizedVideoFrame->height = sourceHeight;
                resizedVideoFrame->format = outputVideoCtx->pix_fmt;
                if(av_image_alloc(resizedVideoFrame->data, resizedVideoFrame->linesize, resizedVideoFrame->width, resizedVideoFrame->height, outputVideoCtx->pix_fmt, 1) <= 0){
                    av_frame_free(&resizedVideoFrame);
                    resizedVideoFrame = nullptr; // \todo memory leak possible
                }
            }
        }
        if(resizedVideoFrame != nullptr){
            sws_scale(scaleCtx, tFrame->data,
                      tFrame->linesize, 0,
                      sourceHeight,
                      resizedVideoFrame->data, resizedVideoFrame->linesize);
        }
    }
    if(resizedVideoFrame == nullptr){
        //        outputFrame = inputFrame;
        if(scaleCtx != nullptr){
            sws_freeContext(scaleCtx);
            scaleCtx = nullptr;
        }
    }else{
        outputFrame = resizedVideoFrame;
    }

    outputPts += av_rescale_q(1, outputVideoStream->codec->time_base, outputVideoStream->time_base);
    outputFrame->pts = outputPts;


    delete[] frame;
    saveVideoFileFrame(outputFrame);
    emit frameProcessed();
}


bool Writer::saveVideoFileFrame(AVFrame *inputFrame)
{
    AVPacket * outputVideoPacket;
    outputVideoPacket = new AVPacket();
    av_init_packet(outputVideoPacket);
    outputVideoPacket->data = NULL;
    outputVideoPacket->size = 0;

    AVFrame * outputVideoFrame=inputFrame;
    outputVideoFrame->quality = 32;

    //    //!TODO maybe in different place
    //    qint64 currSecondFrameNr = (frameNr-1) - wholeSecondsRecorded*fps;
    //    qint64 currFrameTs = recordCurrTimeDVR.toMSecsSinceEpoch() - recordStartTimeDVR.toMSecsSinceEpoch() + currSecondFrameNr*1000/fps;

    //    currVideoFrameTs = inputFrame->pts/(av_q2d(outputVideoStream->time_base)*1000.);

    int ok = 0, got_output = 0;
    ok = avcodec_encode_video2(outputVideoCtx, outputVideoPacket, outputVideoFrame, &got_output);

    if (ok < 0 || !got_output)
    {
        //qDebug() << "Recorder::saveFrame could not encode packet for video [fail] "<< ok << " " << got_output;
        av_free_packet(outputVideoPacket);
        delete outputVideoPacket;
        return true;
    }else{
    }

    outputVideoPacket->stream_index = outputVideoStream->index;
    int write_ret = av_write_frame(outputVideoFmtCtx, outputVideoPacket);
    if (write_ret < 0) {
    }
    av_free_packet(outputVideoPacket);
    delete outputVideoPacket;
    return true;
}

void Writer::finalize()
{
    int ok;
    int got_output;
    AVPacket * outputVideoPacket;
    outputVideoPacket = new AVPacket();
    av_init_packet(outputVideoPacket);
    outputVideoPacket->data = NULL;
    outputVideoPacket->size = 0;
    //    int got_output;
    ok = avcodec_encode_video2(outputVideoCtx, outputVideoPacket, NULL, &got_output);
    if (ok < 0 || !got_output){
        if (ok < 0){
            char *txt = new char[255];
            av_strerror(ok,txt,255);
            qDebug() << "Recorder::saveFrame could not encode video packet [fail]"<< txt << " " << got_output ;
            delete [] txt;
        }
    }else{
        outputVideoPacket->stream_index = outputVideoStream->index;
        int write_ret = av_write_frame(outputVideoFmtCtx, outputVideoPacket);
        if (write_ret < 0){
            qDebug() << "Could not write to disk video ";
        }

    }

    av_write_trailer(outputVideoFmtCtx);
    av_free_packet(outputVideoPacket);
    delete(outputVideoPacket);

    emit finished();
    qDebug() << " emit finished();";
}

int Writer::initOutputVideoFile()
{
    freeOutputVideoFile();

//    outputVideoFmt =  av_guess_format(0, 0, "video/webm");
    outputVideoFmt =  av_guess_format("mpeg2video",0, 0);
//    outputVideoFmt =  av_guess_format("h264",0, 0);

    if(outputVideoFmt == NULL){
        qCritical() << "Recorder::initOutputCtx failed to find video format";
        return -100;
    }

//    outputVideoFmt->video_codec = AV_CODEC_ID_VP9;
//    outputVideoFmt->video_codec = AV_CODEC_ID_MPEG2VIDEO;

    int ret = avformat_alloc_output_context2(&outputVideoFmtCtx,outputVideoFmt,NULL,NULL);
    if (ret < 0) {
        qDebug() << "Error format";
        return ret;
    }

    outputVideoCodec = avcodec_find_encoder(outputVideoFmt->video_codec);
    if (!outputVideoCodec) {
        qCritical() << "Recorder::initOutputCtx failed to alocate video stream";
    }

    // add the video stream using the default format codec and initialize
    outputVideoStream = avformat_new_stream(outputVideoFmtCtx, outputVideoCodec);
    outputVideoStream->time_base.num = 1;
    outputVideoStream->time_base.den = 25;

    outputVideoCtx = outputVideoStream->codec;
    avcodec_get_context_defaults3(outputVideoCtx, outputVideoCodec);
    if(!outputVideoCtx )
    {
        qCritical() << "Recorder::initOutputCtx failed to init output CTX for video";
        return -101;
    }

    outputVideoCtx->codec_type = AVMEDIA_TYPE_VIDEO;
    outputVideoCtx->bit_rate      = 50000000;
    outputVideoCtx->width         = sourceWidth;
    outputVideoCtx->height        = sourceHeight;
    outputVideoCtx->pix_fmt       = *outputVideoCodec->pix_fmts;
    outputVideoCtx->time_base.num = outputVideoStream->time_base.num;
    outputVideoCtx->time_base.den = outputVideoStream->time_base.den;
    outputVideoCtx->framerate     = (AVRational){25, 1};

    outputVideoCtx->qmin = 4;
    outputVideoCtx->qmax = 63;
    outputVideoCtx->thread_count = 4;

    if(outputVideoFmtCtx->oformat->flags & AVFMT_GLOBALHEADER){
        outputVideoCtx->flags |= CODEC_FLAG_GLOBAL_HEADER|AVFMT_NOFILE|AVFMT_FLAG_IGNIDX;
        outputVideoCtx->flags &=~AVFMT_FLAG_GENPTS;
    }

    AVDictionary *opts = NULL;

    av_dict_set(&opts, "crf", "10", 0);
    av_dict_set(&opts, "quality", "realtime", 0);
//    av_dict_set(&opts, "quality", "quality", 0);
    av_dict_set(&opts, "speed", "4", 0);


    ret = avcodec_open2(outputVideoCtx, outputVideoCodec, &opts);
    if (ret < 0) {
        qCritical() << "Recorder::initOutputCtx failed to init output video codec";
        return -101;
    }


    if(avio_open(&outputVideoFmtCtx->pb, filename.toStdString().c_str(), AVIO_FLAG_WRITE) < 0){
        qCritical () << " Failed to open video file " << filename;
        return -105;
    }
    avformat_write_header(outputVideoFmtCtx, NULL);

    av_dump_format(outputVideoFmtCtx, 0, NULL, 1);

    // ======================================================== Video scale context

    scaleCtx = sws_getContext(sourceWidth,
                              sourceHeight,
                              AV_PIX_FMT_RGB24,
                              sourceWidth, sourceHeight,
                              outputVideoCtx->pix_fmt,
                              SWS_AREA,
                              nullptr, nullptr, nullptr);
    if(scaleCtx == nullptr) {
        qWarning() << "Could not initalize scale context, output will not be scaled";
    }

    //    qDebug() << "========== Done init vido file!";
    // freeOutputVideoFile();
    return 0;
}

void Writer::freeOutputVideoFile()
{
    if(outputVideoStream != nullptr){
        av_free(outputVideoStream);
        outputVideoStream = nullptr;
    }

    if(outputVideoFmtCtx != nullptr && outputVideoFmtCtx->pb != nullptr)
        avio_close(outputVideoFmtCtx->pb);

    if(outputVideoCtx != nullptr){
        avcodec_close(outputVideoCtx);
        avcodec_free_context(&outputVideoCtx);
        outputVideoCtx = nullptr;
    }
}










//void Writer::doTerminate()
//{
//    delete logoDetect;
//    delete logoPresence;
//    adHeat.release();

//    if(status == IDLE){
//        emit finished();
//    }else{
//        status = STOPPPING;
//        mustTerminate = true;
//    }

//    av_freep(fifo);

//}

//void Writer::init(MarkupDB::SourceInfo info)
//{
//    currentSource = info;

//    //if(!SmartOpt::getInstance()->dvrConfig.standalone){
//    if(info.endRecordAt > 0){
//        hasRecordEndTime = true;
//        recordEndTime = QDateTime::fromMSecsSinceEpoch(info.endRecordAt);
//    }
//    if(info.startRecordAt > 0){
//        hasDelayedRecordStartTime = true;
//        recordStartTimeDVR = QDateTime::fromMSecsSinceEpoch(info.startRecordAt);
//    }

//    //}
//}

//void Writer::start()
//{
//    status = STARTED;
//    QTimer::singleShot(10,this,&Writer::run);
//}

//long Writer::processMemUsage(qint64 id)
//{
//    long rrss = 0L;
//    FILE* fp = NULL;
//    if ( (fp = fopen( QString("/proc/%1/statm").arg(id).toStdString().c_str(), "r" )) == NULL )
//        return 0 ;      /* Can't open? */
//    if ( fscanf( fp, "%*s%ld", &rrss ) != 1 )
//    {
//        fclose( fp );
//        return 0 ;      /* Can't read? */
//    }
//    fclose( fp );
//    return long(((size_t)rrss * (size_t)sysconf( _SC_PAGESIZE))/1024.0);
//}

//void Writer::run()
//{
//    setSafeToReconnect(false);

//    if(status == STOPPPING){
//        //        qDebug() << "Writer::run STOPPPING with frames in cache: " << frameCache->getFullFrameCount() << " , currently empty: "<< frameCache->getEmptyFrameCount();
//        if(!mustTerminate) emit stopRecording();
//        emit finished();
//        setSafeToReconnect(true);
//        return;
//    }

//    int count = packetQueue->size();
//    if(count == 0){
//        QTimer::singleShot(30,this,&Writer::run);
//        setSafeToReconnect(true);
//        return;
//    }else if(count > 2000){
//        qDebug() << "packetQueue: " <<count << "  av_audio_fifo_size(fifo): " << av_audio_fifo_size(fifo);
//    }

//    while(count-- > 0){

//        AVPacket * pkt = packetQueue->dequeue();

//        int queueSizeAfter = packetQueue->size();

//        if(pkt == nullptr) break;

//        bool ok;
//        if (pkt->stream_index == inputVideoStream->index){
//            ok = decodeVideoFrame(pkt);
//        }

//        if (pkt->stream_index == inputAudioStream->index){
//            ok = decodeAudioFrame(pkt);
//        }
//        av_free_packet(pkt);
//        delete pkt;
//    }

//    setSafeToReconnect(true);
//    QTimer::singleShot(5,this,&Writer::run);
//}

//void Writer::disconnect()
//{
//    if(outputCtx != nullptr){
//        avcodec_close(outputCtx);
//        avcodec_free_context(&outputCtx);
//        delete outputCtx;
//        outputCtx = nullptr;
//    }
//    if(modelScaleCtx != nullptr){
//        sws_freeContext(modelScaleCtx);
//        modelScaleCtx = nullptr;
//    }
//    if(outputModelFrame != nullptr)
//    {
//        av_frame_free(&outputModelFrame);
//        outputModelFrame = nullptr;
//    }
//    if(scaleCtx != nullptr){
//        sws_freeContext(scaleCtx);
//        scaleCtx = nullptr;
//    }
//    if(resizedVideoFrame != nullptr)
//    {
//        av_frame_free(&resizedVideoFrame);
//        resizedVideoFrame = nullptr;
//    }
//    if(resizedThumbFrame != nullptr)
//    {
//        av_frame_free(&resizedThumbFrame);
//        resizedThumbFrame = nullptr;
//    }
//    if(outputPacket != nullptr)
//    {
//        av_free_packet(outputPacket);
//        delete outputPacket;
//        outputPacket = nullptr;
//    }
//}

//bool Writer::decodeVideoFrame(AVPacket *inputPacket)
//{
//    AVFrame* inputFrame;
//    inputFrame  = avcodec_alloc_frame();
//    if (!inputFrame){
//        qDebug() << "Error allocating the frame";
//        return false;
//    }

//    int ok = 0, ok_alt = 0;
//    //    av_frame_free(&inputFrame);
//    //    inputFrame  = avcodec_alloc_frame();

//    ok_alt = avcodec_decode_video2(inputVideoStream->codec, inputFrame, &ok, inputPacket);

//    if(0 == ok || ok_alt < 0)
//    {
//        av_free_packet(inputPacket);
//        droppedFrame = true;
//        droppedFrameCount ++;
//        return false;
//    }
//    if(inputPacket->duration != 0 && inputPacket->duration != (int64_t)AV_NOPTS_VALUE && inputPacket->pts != (int64_t)AV_NOPTS_VALUE/* && inputPacket->dts != (int64_t)AV_NOPTS_VALUE*/){
//        if(nextPts != -1 && inputPacket->pts != nextPts){
//            droppedFrame = true;
//        }else{
//            droppedFrame = false;
//        }
//        nextPts = inputPacket->duration + inputPacket->pts;
//    }else{
//        droppedFrame = false;
//    }

//    droppedFrameCount = 0;

//    saveVideoFrame(inputFrame);

//    av_frame_unref(inputFrame);
//    av_frame_free(&inputFrame);
//    delete inputFrame;

//    av_free_packet(inputPacket);
//}

//bool Writer::decodeAudioFrame(AVPacket *inputPacket)
//{
//    static uint8_t *audio_pkt_data = NULL;
//    static int audio_pkt_size = 0;
//    //static AVFrame frame;

//    int len1, data_size = 0;
//    audio_pkt_data = inputPacket->data;
//    audio_pkt_size = inputPacket->size;

//    if(audio_pkt_size == 0){
//        return false;
//    }

//    AVFrame* inputFrame = avcodec_alloc_frame();
//    if (!inputFrame){
//        qDebug() << "Error allocating the frame";
//        return false;
//    }

//    while(audio_pkt_size > 0) {
//        int got_frame = 0;
//        len1 = avcodec_decode_audio4(inputAudioStream->codec, inputFrame, &got_frame, inputPacket);

//        if(len1 < 0) {
//            /* if error, skip frame */
//            qDebug() << "error audio frame";
//            audio_pkt_size = 0;
//            break;
//        }
//        audio_pkt_data += len1;
//        audio_pkt_size -= len1;
//        if (got_frame)
//        {
//            data_size =
//                    av_samples_get_buffer_size
//                    (
//                        NULL,
//                        inputAudioStream->codec->channels,
//                        inputFrame->nb_samples,
//                        inputAudioStream->codec->sample_fmt,
//                        0
//                        );
//            saveAudioFrame(inputFrame);
//        }else{
//            qDebug() << "missing audio frame";
//            /* no frame*/
//        }

//        if(data_size <= 0) {
//            continue;
//        }else{
//            av_frame_free(&inputFrame);
//            return true;
//        }
//        //!FIXME some strange loop condition
//        av_frame_free(&inputFrame);
//        return false;
//    }
//    av_frame_free(&inputFrame);
//    return true;
//}

//bool Writer::saveVideoFrame(AVFrame *inputFrame)
//{

//    if(status == WAITING_AUDIO_AND_END){
//        return false;
//    }

//    QDateTime currDVRTime = QDateTime::currentDateTime();
//    bool skipFrameMsg = false;

//    if(status == STARTED){
//        recordStartTimeLastSecond = currDVRTime.time().second();
//        status = WAITING_WHOLE_SEC;

//        // Calculate start time so video output can be initialised
//        // {
//        QDateTime expectedStartTime = currDVRTime.addSecs(1);
//        QTime tTime = expectedStartTime.time();
//        tTime.setHMS(
//                    expectedStartTime.time().hour(),
//                    expectedStartTime.time().minute(),
//                    expectedStartTime.time().second(),
//                    0);
//        expectedStartTime.setTime(tTime);
//        if(hasDelayedRecordStartTime && recordStartTimeDVR > expectedStartTime){
//            expectedStartTime = recordStartTimeDVR;
//        }
//        // Output can be initialised if this is reconnect
//        if(!hasOutputInitialised){
//            initOutputVideoFile(expectedStartTime);
//        }
//        // }
//    }

//    if(status == WAITING_WHOLE_SEC){
//        // Must wait for new second to start
//        if(recordStartTimeLastSecond == currDVRTime.time().second()) return false;
//    }

//    // Current frame ts (sometimes multiplied by inputVideoStream->codec->ticks_per_frame)
//    qint64 currPTS = av_frame_get_best_effort_timestamp(inputFrame);


//    if(status == WAITING_WHOLE_SEC){
//        // Timing logic for fragments ============================================
//        // Set MS to 0 when starting
//        QTime tTime = currDVRTime.time();
//        tTime.setHMS(
//                    currDVRTime.time().hour(),
//                    currDVRTime.time().minute(),
//                    currDVRTime.time().second(),
//                    0);
//        currDVRTime.setTime(tTime);


//        if(frameNr == 0){
//            recordCurrTimeDVR = currDVRTime;
//            fragmentStartTime = currDVRTime;

//            fragmentShift = recordCurrTimeDVR.time().second()*fps;
//            qDebug() << "Starting reading .."<< fragmentStartTime.toString("hh.mm.ss");

//        }else{
//            qDebug() << "Starting after recconect"<< fragmentStartTime.toString("hh.mm.ss");

//            //dvrTsLoopCorrection += frameDisplayTime * frameNr;
//            ///FIXME should add skipped frames here (could be done by adjusting dvrTsLoopCorrection later)
//        }

//        if(hasDelayedRecordStartTime && recordStartTimeDVR > recordCurrTimeDVR){
//            status = WAITING_START;
//            videoStartPTS = currPTS;
//            emit sourceStatusChange(currentSource.sourceId,currentSource.status, QString("Waiting start at %1").arg(recordStartTimeDVR.toString("yyyy.MM.dd hh:mm")) );
//        }else{
//            status = WRITING;
//            videoStartPTS = currPTS;
//            videoStartTS = currPTS*av_q2d(inputVideoStream->time_base)*1000;
//            // Only if first time when starting
//            if(frameNr == 0){
//                recordStartTimeDVR = recordCurrTimeDVR;
//                tvModel.setOffsetFrames(0);
//                tvModel.setStartTime(recordStartTimeDVR.toMSecsSinceEpoch());
//            }
//            currentSource.status = MarkupDB::WORKING;
//            emit sourceStatusChange(currentSource.sourceId,currentSource.status, QString("Started recording %1.").arg(recordStartTimeDVR.toString("yyyy.MM.dd hh:mm")) );
//        }
//    }

//    qint64 currTS = double(currPTS-videoStartPTS)*av_q2d(inputVideoStream->time_base)*1000. + dvrTsLoopCorrection;


//    // Recording logic =======================================================
//    double requiredFrames = double(currTS - frameDisplayTime*frameNr)/(double)frameDisplayTime;
//    if(requiredFrames <= 0.5 ){
//        //        qDebug() << "-------------------- ignore frame ------------------------------------- ";
//    }

//    if(requiredFrames < -0.5 ){
//        // This happens when video file is reopened
//        qint64 loopCorrrectionDiff = (requiredFrames*(-1.)+0.6)*frameDisplayTime;
//        dvrTsLoopCorrection += loopCorrrectionDiff;
//        currTS = double(currPTS-videoStartPTS)*av_q2d(inputVideoStream->time_base)*1000. + dvrTsLoopCorrection;
//        requiredFrames = double(currTS - frameDisplayTime*frameNr)/(double)frameDisplayTime;
//    }

//    int dupl = 0;

//    while(requiredFrames>0.5){
//        frameNr++;
//        requiredFrames = double(currTS - frameDisplayTime*frameNr)/(double)frameDisplayTime;
//        //                qDebug() << "- " << currTS << requiredFrames << frameNr << frameDisplayTime << videoStartPTS << currTS << currPTS;

//        // Timing logic DVR time -----------------------------------------------
//        // Frame number in current second 0 -- fps
//        qint64 currSecondFrameNr = (frameNr-1) - wholeSecondsRecorded*fps;
//        qint64 currFrameInFragment = (frameNr-1) + fragmentShift - wholeFragmentsRecorded * fragmentLength;

//        if( currSecondFrameNr >= fps){
//            // New second recorded
//            wholeSecondsRecorded++;
//            currSecondFrameNr = 0;
//            recordCurrTimeDVR = recordCurrTimeDVR.addSecs(1);
//        }

//        //        qDebug() << currSecondFrameNr << "   " << currFrameInFragment << "  " << recordCurrTimeDVR.toString("mm.ss");


//        if(status != WAITING_START && hasRecordEndTime && recordCurrTimeDVR >= recordEndTime){
//            //End Recording
//            qDebug() << "========= Ending recording, reached recordEndTime ==========";
//            if(fragmentStartTime != recordCurrTimeDVR){
//                qDebug() << "FragmentDone";
//                //!FIXME fragment done
//                status = WAITING_AUDIO_AND_END;

//                outputPtsOffset = outputPts;

//                lastFragmentStartTime = fragmentStartTime;
//                lastFragmentEndTime = recordCurrTimeDVR;
//                finalizeModel();
//                //onFragmentDone(fragmentStartTime,recordCurrTimeDVR);
//            }
//            //            status = STOPPPING;
//            return false;
//        }

//        // Check if fragment done
//        if(currFrameInFragment >= fragmentLength){
//            wholeFragmentsRecorded += 1;
//            if(status == WAITING_AUDIO){
//                qWarning() << "DVR waiting on audio for more than a minute!!!";
//                //!FIXME handle this condition
//            }

//            if(status != WAITING_START){
//                //                qDebug() << ", currFrameInFragment " << currFrameInFragment
//                //                         << ", wholeFragmentsRecorded " << wholeFragmentsRecorded
//                //                         << ", outputPts " << outputPts
//                //                         << ", recordCurrTimeDVR " << recordCurrTimeDVR.toString()
//                //                         << ", lastAudioTs " << lastAudioTs;
//                status = WAITING_AUDIO;

//                outputPtsOffset = outputPts;

//                lastFragmentStartTime = fragmentStartTime;
//                lastFragmentEndTime = recordCurrTimeDVR;
//                finalizeModel();
//            }

//            currFrameInFragment = 0;
//            fragmentStartTime = recordCurrTimeDVR;

//        }

//        // Check if must start (reached recordStartTimeDVR)
//        if(status == WAITING_START){
//            if(recordStartTimeDVR > recordCurrTimeDVR) continue;
//            qDebug() << "=============== Starting record after waiting =============";
//            // OK can start recording now
//            status = WRITING;
//            videoStartPTS = currPTS;
//            videoStartTS = currPTS*av_q2d(inputVideoStream->time_base)*1000;
//            tvModel.setOffsetFrames(currFrameInFragment);
//            tvModel.setStartTime(fragmentStartTime.toMSecsSinceEpoch());
//            patternHandler.clear();
//            currentSource.status = MarkupDB::WORKING;
//            emit sourceStatusChange(currentSource.sourceId,currentSource.status, QString("Started recording ..") );
//        }

//        // Could be optimized
//        makeSecDir(recordCurrTimeDVR);
//        //!TODO calculate skipped frames

//        if(requiredFrames > 4.) droppedFrame = true;
//        if(requiredFrames>1.5 && droppedFrame){
//            if(!skipFrameMsg){
//                qDebug() << "SKIP ================== " << requiredFrames;
//                skipFrameMsg = true;
//            }

//            qDebug() << "Frame skipped! ";
//            skippedFrames ++;
//            tvModel.pushEmptyFrame();
//            patternHandler.pushEmptyFrame();
//            outputPts += av_rescale_q(1, outputVideoStream->codec->time_base, outputVideoStream->time_base);
//            audioOffsetForFrameDrop += av_rescale_q(1, outputVideoStream->codec->time_base, outputVideoStream->time_base);//*av_q2d(outputAudioCtx->time_base)*1000;
//            //// Dont duplicate frames if frames have been dropped, yet this does not work when poor connection
//            continue;
//        }

//        dupl++;
//        //        if(dupl>1)  qDebug() << "DUPLICATE ================== " << requiredFrames << dupl;



//        if(currentSource.status == MarkupDB::LOADING){
//            currentSource.status = MarkupDB::WORKING;
//            emit sourceStatusChange(currentSource.sourceId,currentSource.status, "First data received");
//        }

//        AVFrame * outputFrame = inputFrame;

//        // Resize frame for thumbnail =================================================================================

//        if(thumbScaleCtx != nullptr) {
//            if(resizedThumbFrame == nullptr){
//                resizedThumbFrame = av_frame_alloc();
//                if(resizedThumbFrame != nullptr){
//                    resizedThumbFrame->height = outputCtx->height;
//                    resizedThumbFrame->width = outputCtx->width;
//                    resizedThumbFrame->format = outputCtx->pix_fmt;
//                    if(av_image_alloc(resizedThumbFrame->data, resizedThumbFrame->linesize, resizedThumbFrame->width, resizedThumbFrame->height, outputCtx->pix_fmt, 1) <= 0){
//                        av_frame_free(&resizedThumbFrame);
//                        delete resizedThumbFrame;
//                    }
//                }
//            }
//            if(resizedThumbFrame != nullptr){
//                sws_scale(thumbScaleCtx, inputFrame->data,
//                          inputFrame->linesize, 0,
//                          sourceHeight,
//                          resizedThumbFrame->data, resizedThumbFrame->linesize);
//            }
//        }
//        if(resizedThumbFrame == nullptr){
//            outputFrame = inputFrame;
//            if(thumbScaleCtx != nullptr){
//                sws_freeContext(thumbScaleCtx);
//                thumbScaleCtx = nullptr;
//            }
//        }else{
//            outputFrame = resizedThumbFrame;
//        }

//        /// adding frame info to pattern model
//        patternHandler.handle(outputFrame);

//        /// detecting logo and collecting results for each frame

//        logoConfidence  = logoDetect->handler(outputFrame); /// call tvlogo and get percent

//        ////        qDebug() << "advert confidence: " << logoConfidence; //for debug only
//        if (logoConfidence == -2)
//        {
//            adHeat.at<Vec3b>(0, currFrameInFragment)[0] = 0;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[1] = 0;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[2] = 255;
//        }
//        else if (logoConfidence == -1)
//        {
//            adHeat.at<Vec3b>(0, currFrameInFragment)[0] = 255;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[1] = 0;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[2] = 0;
//        }
//        else if (logoConfidence>=0 && logoConfidence <256)
//        {
//            adHeat.at<Vec3b>(0, currFrameInFragment)[0] = logoConfidence;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[1] = logoConfidence;
//            adHeat.at<Vec3b>(0, currFrameInFragment)[2] = logoConfidence;
//        }
//        outputFrame->pts = frameNr;
//        outputFrame->quality = 3;

//        outputFrame->quality = outputCtx->global_quality;

//        int ok = 0, got_output = 0;
//        ok = avcodec_encode_video2(outputCtx, outputPacket, outputFrame, &got_output);


//        if (ok < 0 || !got_output)
//        {
//            qDebug() << "Recorder::saveFrame could not encode packet [fail]";
//            av_free_packet(outputPacket);
//            delete outputPacket;
//            return false;
//        }


//        // Save thumb to disk -----------------------------------------------------------------------------

//        QString path = QString("%1%2/%3.jpg").arg(SmartOpt::getInstance()->dvrConfig.savePath).arg(getSecondFolderRelativePath(recordCurrTimeDVR)).arg(currSecondFrameNr, 2, 10, QChar('0'));

//        // For testing only:
//        //QString path = QString("%1/tmp/%3.jpg").arg(SmartOpt::getInstance()->dvrConfig.savePath).arg(wholeSecondsRecorded*fps+currSecondFrameNr, 5, 10, QChar('0'));
//        //qDebug() << wholeSecondsRecorded*fps+currSecondFrameNr;
//        //qDebug() << "Saving: " << path;
//        //path = QString("%1/%2.jpg").arg("/home/rotjix/temp/test").arg(wholeSecondsRecorded*fps+currSecondFrameNr, 5, 10, QChar('0'));
//        //qDebug() << wholeSecondsRecorded*fps+currSecondFrameNr << " xxx " << frame.frame->pkt_pts;

//        FILE* f = fopen(path.toStdString().c_str(), "wb");
//        if(f == 0){
//            av_free_packet(outputPacket);
//            delete outputPacket;
//            //            qDebug() << "Could not write to disk";
//            return false;
//        }
//        fwrite(outputPacket->data, 1, outputPacket->size, f);
//        fclose(f);

//        av_free_packet(outputPacket);
//        goodFrames++;


//        // AdModel =================================================================================


//        DVROpt opt = SmartOpt::getInstance()->dvrConfig;
//        if(modelScaleCtx != nullptr) {
//            if(outputModelFrame == nullptr){
//                outputModelFrame = av_frame_alloc();
//                if(outputModelFrame != nullptr){
//                    outputModelFrame->height = opt.adModelConfig.height;
//                    outputModelFrame->width = opt.adModelConfig.width;
//                    outputModelFrame->format = AV_PIX_FMT_GRAY8;
//                    if(av_image_alloc(outputModelFrame->data, outputModelFrame->linesize,
//                                      outputModelFrame->width, outputModelFrame->height, AV_PIX_FMT_GRAY8, 1) <= 0){
//                        av_frame_free(&outputModelFrame);
//                        delete outputModelFrame;
//                        outputModelFrame = nullptr; // \todo memory leak possible
//                    }
//                }
//            }
//            if(outputModelFrame != nullptr){
//                sws_scale(modelScaleCtx, inputFrame->data,
//                          inputFrame->linesize, 0,
//                          sourceHeight,
//                          outputModelFrame->data, outputModelFrame->linesize);
//                if(outputModelFrame != nullptr){
//                    tvModel.pushFrame(outputModelFrame->data[0]);
//                }
//            }
//        }


//        // Video =================================================================================


//        if(scaleCtx != nullptr) {
//            if(resizedVideoFrame == nullptr){
//                resizedVideoFrame = av_frame_alloc();
//                if(resizedVideoFrame != nullptr){
//                    resizedVideoFrame->width = videoWidth;
//                    resizedVideoFrame->height = videoHeight;
//                    resizedVideoFrame->format = outputVideoCtx->pix_fmt;
//                    if(av_image_alloc(resizedVideoFrame->data, resizedVideoFrame->linesize, resizedVideoFrame->width, resizedVideoFrame->height, outputVideoCtx->pix_fmt, 1) <= 0){
//                        av_frame_free(&resizedVideoFrame);
//                        resizedVideoFrame = nullptr; // \todo memory leak possible
//                    }
//                }
//            }
//            if(resizedVideoFrame != nullptr){
//                sws_scale(scaleCtx, inputFrame->data,
//                          inputFrame->linesize, 0,
//                          sourceHeight,
//                          resizedVideoFrame->data, resizedVideoFrame->linesize);
//            }
//        }
//        if(resizedVideoFrame == nullptr){
//            outputFrame = inputFrame;
//            if(scaleCtx != nullptr){
//                sws_freeContext(scaleCtx);
//                scaleCtx = nullptr;
//            }
//        }else{
//            outputFrame = resizedVideoFrame;
//        }

//        outputPts += av_rescale_q(1, outputVideoStream->codec->time_base, outputVideoStream->time_base);
//        outputFrame->pts = outputPts - outputPtsOffset;//* av_q2d(outputVideoStream->time_base);

//        if(status != WAITING_AUDIO_AND_END && status != WAITING_AUDIO && videoFrameQueue.size() > 0){
//            while(videoFrameQueue.size() > 0){
//                AVFrame *tFrame = videoFrameQueue.dequeue();
//                //                qDebug() << "Frame dequed";
//                //                qDebug() << "videoFrameQueue.size()" << videoFrameQueue.size();
//                saveVideoFileFrame(tFrame);
//                av_frame_unref(tFrame);
//                av_frame_free(&tFrame);
//                delete tFrame;
//            }
//        }

//        //        qDebug() << "packetQueueSize: " << packetQueue->size();
//        //        qDebug() << "frameQueueSize: " << videoFrameQueue.size();
//        //        qDebug() << "av" <<av_audio_fifo_size(fifo);
//        saveVideoFileFrame(outputFrame);
//    }
//    return true;
//}


//bool Writer::saveAudioFrame(AVFrame* inputFrame)
//{
//    if(!hasOutputInitialised) return false;
//    int error;
//    if(inputFrame != 0){
//        if(inputFrame->nb_samples == 0 ){
//            qDebug() << "Empty input audio frame";
//            return false;
//        }

//        uint8_t **converted_data = NULL;
//        uint8_t ***converted_data_ptr = &converted_data;


//        if (!(*converted_data_ptr = (uint8_t **)calloc(outputAudioCtx->channels,
//                                                       sizeof(**converted_data_ptr)))) {
//            qDebug () << "Could not allocate converted input sample pointers";
//            return false;
//        }


//        double inputDuration = inputFrame->pkt_duration;
//        if(inputAudioStream->codec->time_base.den != 0){
//            inputDuration = av_rescale_q(inputDuration, inputAudioStream->time_base,inputAudioStream->codec->time_base);
//        }else{
//            inputDuration = inputFrame->nb_samples;
//        }
//        int dst_nb_samples  = av_rescale_rnd(inputDuration, outputAudioCtx->sample_rate, inputAudioStream->codec->sample_rate, AV_ROUND_UP);
//        //int dst_nb_samples2 = av_rescale_rnd(inputDuration/2, outputAudioCtx->sample_rate, inputAudioStream->codec->sample_rate, AV_ROUND_UP);
//        if(dst_nb_samples < 0){
//            qDebug () << "dst_nb_samples < 0";
//            return false;
//        }

//        /**
//         * Allocate memory for the samples of all channels in one consecutive
//         * block for convenience.
//         */
//        if ((error = av_samples_alloc(converted_data, NULL,
//                                      outputAudioCtx->channels,
//                                      dst_nb_samples,
//                                      outputAudioCtx->sample_fmt, 0)) < 0) {
//            qDebug () << "Could not allocate converted input samples (error '%s')" << error << dst_nb_samples;
//            av_freep(&converted_data[0]);
//            free(*converted_data);
//            return false;
//        }


//        /** Convert the samples using the resampler. */
//        if ((error = swr_convert(resample_context,
//                                 converted_data, dst_nb_samples,
//                                 (const uint8_t **)inputFrame->extended_data    , inputFrame->nb_samples)) < 0) {
//            qDebug()<< "Could not convert input samples (error '%s')\n" <<   error;
//            av_freep(&converted_data[0]);
//            free(*converted_data);
//            return false;
//        }


//        /** Store the new samples in the FIFO buffer. */
//        if (av_audio_fifo_write(fifo, (void **)converted_data, dst_nb_samples) < dst_nb_samples) {
//            qDebug() << "Could not write data to FIFO";
//            //            av_freep(&(*converted_data)[0]);
//            av_freep(&converted_data[0]);
//            free(*converted_data); ////check memory leak here
//            return false;
//        }

//        av_freep(&converted_data[0]);
//        free(*converted_data);////check memory leak here - solved?
//        free(converted_data);
//        //        delete converted_data;

//        // First valid frame stored in fifo
//        if(audioStartTS == std::numeric_limits < qint64 >::max()){
//            audioStartTS = av_frame_get_best_effort_timestamp(inputFrame)*av_q2d(inputAudioStream->time_base)*1000; ///S
//        }

//        if(status == WAITING_START  || status == WAITING_WHOLE_SEC){
//            return true;
//        }

//    }else{
//        //        qDebug() << "audio save required when inputFrame == 0";
//    }


//    if(videoStartPTS == std::numeric_limits < qint64 >::max()) {
//        return false;
//    }else if(audiStartTsDiff == std::numeric_limits < qint64 >::max()){
//        audiStartTsDiff =  videoStartTS - audioStartTS;
//        lastAudioPtsOffset = audiStartTsDiff/(av_q2d(outputAudioCtx->time_base)*1000.);
//        //        qDebug() << "======= audiStartTsDiff " << (long)audiStartTsDiff
//        //                 << " lastAudioTs " << (long)lastAudioTs
//        //                 << " lastAudioPtsOffset ======= "  << (long)lastAudioPtsOffset
//        //                 << " startPTS " << long (lastAudioTs - lastAudioPtsOffset + audiPtsLoopCorrection)
//        //                 << " start sec2 " << long ((lastAudioTs + outputAudioCtx->frame_size - lastAudioPtsOffset + audiPtsLoopCorrection)*av_q2d(outputAudioCtx->time_base)*1000)
//        //                 << " currVideoFrameTs " << long (currVideoFrameTs)
//        //                 << " audiPtsLoopCorrection " << audiPtsLoopCorrection;
//    }

//    /** Temporary storage of the output samples of the frame written to the file. */
//    AVFrame *output_frame_resized;
//    const int frame_size = outputAudioCtx->frame_size;
//    if (!(output_frame_resized = av_frame_alloc())) {
//        qDebug() << "Could not allocate output frame";
//        return false;
//    }
//    output_frame_resized->nb_samples     = frame_size;
//    output_frame_resized->channel_layout = outputAudioCtx->channel_layout;
//    output_frame_resized->format         = outputAudioCtx->sample_fmt;
//    output_frame_resized->sample_rate    = outputAudioCtx->sample_rate;

//    /**
//     * Allocate the samples of the created frame. This call will make
//     * sure that the audio frame can hold as many samples as specified.
//     */
//    if ((error = av_frame_get_buffer(output_frame_resized, 0)) < 0) {
//        qDebug() << "Could allocate output frame samples. error :" << error << frame_size;
//        av_frame_free(&output_frame_resized);
//        return false;
//    }

//    AVPacket * outputAudioPacket = new AVPacket();
//    av_init_packet(outputAudioPacket);
//    outputAudioPacket->data = NULL;
//    outputAudioPacket->size = 0;

//    //!TODO fix this because currently this cant happen (we dont split frames)
//    /**
//     * Use the maximum number of possible samples per frame.
//     * If there is less than the maximum possible frame size in the FIFO
//     * buffer use this number. Otherwise, use the maximum possible frame size
//     * const int frame_size = FFMIN(av_audio_fifo_size(fifo), outputAudioCtx->frame_size);
//     */

//    //    qDebug() << "---";
//    bool hasOutAudio = true;
//    while(hasOutAudio){
//        // Next frame ts
//        qint64 audioTsOut = (lastAudioTs + outputAudioCtx->frame_size - lastAudioPtsOffset + audiPtsLoopCorrection)*av_q2d(outputAudioCtx->time_base)*1000;

//        if((status == WAITING_AUDIO || status == WAITING_AUDIO_AND_END)
//                && audioTsOut  >= lastFragmentStartTime.secsTo(lastFragmentEndTime)*1000){
//            lastAudioPtsOffset = lastAudioTs + outputAudioCtx->frame_size;
//            audiPtsLoopCorrection = 0;

//            finalizeFragment();

//        }

//        //        qDebug() << "Output pts" << audioTsOut << lastFragmentStartTime.secsTo(lastFragmentEndTime)*1000 << " " << av_audio_fifo_size(fifo);

//        if(status == WAITING_AUDIO_AND_END ){
//            qDebug() << "WAITING_AUDIO_AND_END " << av_audio_fifo_size(fifo);
//        }

//        // Check if buffer has full frames
//        if(av_audio_fifo_size(fifo) < outputAudioCtx->frame_size){
//            //            av_frame_unref(output_frame_resized);
//            av_free_packet(outputAudioPacket);
//            delete outputAudioPacket;
//            av_frame_free(&output_frame_resized);
//            return false;
//        }
//        //qDebug() << "outputAudioCtx->frame_size" << outputAudioCtx->frame_size;
//        /**
//         * Set the frame's parameters, especially its size and format.
//         * av_frame_get_buffer needs this to allocate memory for the
//         * audio samples of the frame.
//         * Default channel layouts based on the number of channels
//         * are assumed for simplicity.
//         */
//        qint64 currPTS = av_frame_get_best_effort_timestamp(inputFrame);
//        qint64 audioTS = currPTS*av_q2d(inputAudioStream->time_base)*1000;

//        lastAudioTs += frame_size;

//        if(audioTSOffset == std::numeric_limits < qint64 >::max()){
//            audioTSOffset = 0;
//            if(videoStartTS > audioTS){
//                av_free_packet(outputAudioPacket);
//                delete(outputAudioPacket);
//                av_frame_free(&output_frame_resized);
//                return false;
//            }else if(lastAudioTs == 0){
//                lastAudioPtsOffset = lastAudioTs;
//                audioTSOffset = 0;
//            }
//        }


//        qint64 pts = lastAudioTs - lastAudioPtsOffset + audiPtsLoopCorrection;
//        output_frame_resized->pts = pts;


//        /**
//         * Read as many samples from the FIFO buffer as required to fill the frame.
//         * The samples are stored in the frame temporarily.
//         */
//        int encoded = av_audio_fifo_read(fifo, (void **)output_frame_resized->data, frame_size);

//        if (encoded < frame_size) {
//            qWarning() << "Could not read data from FIFO";
//            av_free_packet(outputAudioPacket);
//            delete outputAudioPacket;
//            av_frame_free(&output_frame_resized);
//            return false;
//        }

//        if ( pts <= 0) {
//            //            qDebug() << "skip Start";
//            currAudioFrameTs = audioTsOut;
//            if(audioTsOut >= currVideoFrameTs){
//                hasOutAudio = false;
//            }

//            continue;
//        }

//        // Fingerprinting

//        int plane_size= av_get_bytes_per_sample(AV_SAMPLE_FMT_FLTP);
//        for(int i=0; i<frame_size; i+=plane_size){
////            float x=0;// = (*output_frame_resized->data)[i];
//            float *y = (float *)((*output_frame_resized->data)+i);
////            copy((*output_frame_resized->data)+i, (*output_frame_resized->data)+i+plane_size, &x);
//            qDebug() << "== " << *y;
////                out[i]= in + i*plane_size;
//            }

////        for(int fr = 0; fr < frame_size; fr ++){

////        }
////        qDebug() << plane_size;

//        // Fingerprinting

//        int ok = 0, got_output = 0;
//        ok = avcodec_encode_audio2(outputAudioCtx, outputAudioPacket, output_frame_resized, &got_output);
//        if (ok < 0 || !got_output)
//        {
//            if (ok < 0){
//                char *txt = new char[255];
//                av_strerror(ok,txt,255);
//                qDebug() << "Recorder::saveFrame could not encode audio packet [fail]"<< txt << " " << got_output;
//                delete [] txt;
//            }
//        }else{

//            outputAudioPacket->stream_index = outputAudioStream->index;
//            av_packet_rescale_ts(outputAudioPacket, outputAudioStream->codec->time_base, outputAudioStream->time_base);
//            outputAudioPacket->duration =  av_rescale_q(outputAudioPacket->duration, outputAudioStream->codec->time_base, outputAudioStream->time_base);

//            int write_ret = av_write_frame(outputVideoFmtCtx, outputAudioPacket);
//            if (write_ret < 0) {
//                //                qDebug() << "Could not write to disk audio " << pts;
//            }
//        }

//        if(audioTsOut >= currVideoFrameTs){
//            hasOutAudio = false;
//        }
//        av_free_packet(outputAudioPacket);
//    }
//    av_free_packet(outputAudioPacket);
//    delete outputAudioPacket;

//    av_frame_free(&output_frame_resized);
//    return true;
//}

//bool Writer::makeSecDir(QDateTime time)
//{
//    QDir dir;
//    return dir.mkpath(SmartOpt::getInstance()->dvrConfig.savePath + getSecondFolderRelativePath(time));
//}

//QString Writer::getSecondFolderRelativePath(QDateTime time)
//{
//    return (time.toString("/%1/yyyy.MM.dd/hh.mm.ss")).arg(currentSource.sourceId);
//}

//QString Writer::getVideoFileName(QDateTime time)
//{
//    QDir dir;
//    QString videoFilePath = SmartOpt::getInstance()->dvrConfig.videoSavePath + time.toString("/%1/yyyy.MM.dd").arg(currentSource.sourceId);
//    dir.mkpath(videoFilePath);
//    videoFilePath.append(time.toString("/hh.mm.web"));
//    videoFilePath.append("m");
//    return videoFilePath;
//}

//bool Writer::makeAdModelDir(QDateTime time)
//{
//    QDir dir;
//    return dir.mkpath(SmartOpt::getInstance()->dvrConfig.adModelConfig.savePath + getAdModelFolderRelativePath(time));
//}

//bool Writer::makeLogoDir(QDateTime time)
//{
//    QDir dir;
//    return dir.mkpath(SmartOpt::getInstance()->dvrConfig.adModelConfig.adSavePath + getAdModelFolderRelativePath(time));
//}

//bool Writer::makePatternDir(QDateTime time)
//{
//    QDir dir;
//    return dir.mkpath(SmartOpt::getInstance()->dvrConfig.adModelConfig.patternSavePath + getAdModelFolderRelativePath(time));
//}

//QString Writer::getAdModelFolderRelativePath(QDateTime time)
//{
//    return (time.toString("/hh.mm/%1/yyyy.MM.dd")).arg(currentSource.sourceId);
//}

//void Writer::initOutputCtx(AVStream *inputVideoStream, AVStream *inputAudioStream)
//{
//    disconnect();
//    //    if(hasOutputInitialised )
//    //        qDebug() << "======* audiStartTsDiff " << (long)audiStartTsDiff
//    //                 << " lastAudioTs " << (long)lastAudioTs
//    //                 << " lastAudioPtsOffset ======= "  << (long)lastAudioPtsOffset
//    //                 << " breakPTS " << long (lastAudioTs - lastAudioPtsOffset + audiPtsLoopCorrection)
//    //                 << " beak sec " << long ((lastAudioTs - lastAudioPtsOffset + audiPtsLoopCorrection)*av_q2d(outputAudioCtx->time_base)*1000)
//    //                 << " currVideoFrameTs " << long (currVideoFrameTs)
//    //                 << " audiPtsLoopCorrection " << audiPtsLoopCorrection;


//    // Add silence at video end if required
//    // {
//    if(hasOutputInitialised && currAudioFrameTs < currVideoFrameTs){
//        AVFrame *output_frame_resized;
//        int error;
//        const int frame_size = outputAudioCtx->frame_size;
//        if (!(output_frame_resized = av_frame_alloc())) {
//            qDebug() << "Could not allocate output frame";
//            currAudioFrameTs = currVideoFrameTs;
//        }else{
//            output_frame_resized->nb_samples     = frame_size;
//            output_frame_resized->channel_layout = outputAudioCtx->channel_layout;
//            output_frame_resized->format         = outputAudioCtx->sample_fmt;
//            output_frame_resized->sample_rate    = outputAudioCtx->sample_rate;

//            /**
//             * Allocate the samples of the created frame. This call will make
//             * sure that the audio frame can hold as many samples as specified.
//             */
//            if ((error = av_frame_get_buffer(output_frame_resized, 0)) < 0) {
//                qDebug() << "Could allocate output frame samples. error :" << error << frame_size;
//                //        av_frame_unref(output_frame_resized);
//                av_frame_free(&output_frame_resized);
//                currAudioFrameTs = currVideoFrameTs;
//            }
//        }
//        AVPacket * outputAudioPacket = new AVPacket();
//        av_init_packet(outputAudioPacket);
//        outputAudioPacket->data = NULL;
//        outputAudioPacket->size = 0;

//        qDebug() <<  "added  audio silence " << currVideoFrameTs - currAudioFrameTs << " ms ";
//        while(currAudioFrameTs < currVideoFrameTs){
//            qint64 audioTsOut = (lastAudioTs + outputAudioCtx->frame_size - lastAudioPtsOffset + audiPtsLoopCorrection)*av_q2d(outputAudioCtx->time_base)*1000;
//            lastAudioTs += frame_size;
//            qint64 pts = lastAudioTs - lastAudioPtsOffset + audiPtsLoopCorrection;
//            output_frame_resized->pts = pts;
//            int ok = 0, got_output = 0;

//            ok = avcodec_encode_audio2(outputAudioCtx, outputAudioPacket, output_frame_resized, &got_output);
//            if (ok < 0 || !got_output)
//            {
//                if (ok < 0){
//                    char *txt = new char[255];
//                    av_strerror(ok,txt,255);
//                    qDebug() << "Recorder::saveFrame could not encode audio packet [fail]"<< txt << " " << got_output;
//                    delete[] txt;
//                }
//            }else{

//                outputAudioPacket->stream_index = outputAudioStream->index;
//                av_packet_rescale_ts(outputAudioPacket, outputAudioStream->codec->time_base, outputAudioStream->time_base);
//                outputAudioPacket->duration =  av_rescale_q(outputAudioPacket->duration, outputAudioStream->codec->time_base, outputAudioStream->time_base);

//                int write_ret = av_write_frame(outputVideoFmtCtx, outputAudioPacket);
//                if (write_ret < 0) {
//                    qDebug() << "Could not write to disk audio " <<pts;
//                }
//            }
//            currAudioFrameTs = audioTsOut;
//            av_free_packet(outputAudioPacket);
//        }

//        av_free_packet(outputAudioPacket);
//        delete outputAudioPacket;
//        av_frame_free(&output_frame_resized);
//        delete output_frame_resized;
//    }
//    // }

//    audiPtsLoopCorrection  = (lastAudioPtsOffset==std::numeric_limits < qint64 >::max())?0:-lastAudioPtsOffset;
//    lastAudioPtsOffset = 0;

//    videoStartPTS = std::numeric_limits < qint64 >::max();
//    audiStartTsDiff = std::numeric_limits < qint64 >::max();
//    audioTSOffset = std::numeric_limits < qint64 >::max();
//    audioStartTS = std::numeric_limits < qint64 >::max();
//    if(fifo != 0) av_audio_fifo_reset(fifo);

//    this->inputVideoStream = inputVideoStream;
//    this->inputAudioStream = inputAudioStream;

//    sourceWidth = inputVideoStream->codec->width;
//    sourceHeight = inputVideoStream->codec->height;
//    status = STARTED;

//    DVROpt opt = SmartOpt::getInstance()->dvrConfig;

//    // ======================================================== Video scale context

//    // Warning! - variables reused
//    double wScale = (opt.maxWidth > 0)? ((double)opt.maxWidth/sourceWidth) :-1.;
//    double hScale = (opt.maxHeight > 0)? ((double)opt.maxHeight/sourceHeight) :-1.;
//    double scale = std::max(wScale,hScale);
//    videoWidth = sourceWidth;
//    videoHeight = sourceHeight;
//    if(scale > 0. && scale < 1.){
//        videoWidth = (double)sourceWidth*scale;
//        videoHeight = (double)sourceHeight*scale;

//        if(videoWidth%2 != 0) videoWidth -= 1;
//        if(videoHeight%2 != 0) videoHeight -= 1;

//        scaleCtx = sws_getContext(sourceWidth,
//                                  sourceHeight,
//                                  inputVideoStream->codec->pix_fmt,
//                                  videoWidth, videoHeight,
//                                  AV_PIX_FMT_YUVJ420P,
//                                  SWS_AREA,
//                                  nullptr, nullptr, nullptr);
//        if(scaleCtx == nullptr) {
//            qWarning() << "Could not initalize scale context, output will not be scaled";
//        }
//    }

//    // ======================================================== Video scale context

//    wScale = (opt.thumbWidth > 0)? ((double)opt.thumbWidth/sourceWidth) :-1.;
//    hScale = (opt.thumbHeight > 0)? ((double)opt.thumbHeight/sourceHeight) :-1.;
//    scale = std::max(wScale,hScale);
//    thumbWidth = sourceWidth;
//    thumbHeight = sourceHeight;
//    if(scale > 0. && scale < 1.){
//        thumbWidth = (double)sourceWidth*scale;
//        thumbHeight = (double)sourceHeight*scale;

//        if(thumbWidth%2 != 0) thumbWidth -= 1;
//        if(thumbHeight%2 != 0) thumbHeight -= 1;

//        thumbScaleCtx = sws_getContext(sourceWidth,
//                                       sourceHeight,
//                                       inputVideoStream->codec->pix_fmt,
//                                       thumbWidth, thumbHeight,
//                                       AV_PIX_FMT_YUVJ420P,
//                                       SWS_AREA,
//                                       nullptr, nullptr, nullptr);
//        if(thumbScaleCtx == nullptr) {
//            qWarning() << "Could not initalize scale context, output will not be scaled";
//        }
//    }

//    // ======================================================== Thumb output context

//    outputCodec = avcodec_find_encoder(AV_CODEC_ID_MJPEG);
//    outputCtx = avcodec_alloc_context3(outputCodec);
//    if(!outputCtx )
//    {
//        qDebug() << "Recorder::initOutputCtx failed to init output CTX";
//        return;
//    }


//    outputCtx->qmin = 12;
//    outputCtx->qmax = 40;
//    outputCtx->bit_rate      = 120000;
//    outputCtx->width         = thumbWidth;
//    outputCtx->height        = thumbHeight;
//    outputCtx->pix_fmt       = AV_PIX_FMT_YUVJ420P;
//    outputCtx->time_base.num = 1;
//    outputCtx->time_base.den = fps;

//    outputCtx->mb_lmin        = outputCtx->lmin = outputCtx->qmin * FF_QP2LAMBDA;
//    outputCtx->mb_lmax        = outputCtx->lmax = outputCtx->qmax * FF_QP2LAMBDA;
//    outputCtx->flags          = CODEC_FLAG_QSCALE;
//    outputCtx->global_quality = outputCtx->qmin * FF_QP2LAMBDA;

//    if (avcodec_open2(outputCtx, outputCodec, nullptr) < 0) {
//        qCritical() << "Recorder::initOutputCtx failed to init output codec";
//        return;
//    }

//    outputPacket = new AVPacket();
//    av_init_packet(outputPacket);
//    outputPacket->data = nullptr;
//    outputPacket->size = 0;

//    // ======================================================== Model scale context

//    // Model creation context
//    if(opt.adModelConfig.createModel){
//        modelScaleCtx = sws_getContext(sourceWidth,
//                                       sourceHeight,
//                                       inputVideoStream->codec->pix_fmt,
//                                       opt.adModelConfig.width,
//                                       opt.adModelConfig.height,
//                                       AV_PIX_FMT_GRAY8,
//                                       SWS_AREA,
//                                       nullptr, nullptr, nullptr);
//        if(modelScaleCtx == nullptr) {
//            qWarning() << "Could not initalize scale context, output will not be scaled";
//        }
//    }
//}

//void Writer::finalizeModel()
//{
//    //    emit sourceStatusChange(currentSource.sourceId,currentSource.status,
//    //                            QString("Fragment saved [%1-%2]")
//    //                            .arg(start.toString("hh.mm"))
//    //                            .arg(end.toString("hh.mm")) );
//    makeAdModelDir(fragmentStartTime);
//    QString filename = QString("%1%2/ad.model").arg(SmartOpt::getInstance()->dvrConfig.adModelConfig.savePath).arg(getAdModelFolderRelativePath(fragmentStartTime));


//    tvModel.writeToFile(filename);
//    tvModel.reset(fps,recordCurrTimeDVR.toMSecsSinceEpoch(),0);

//    lastFragmentSkippedFrames = skippedFrames;
//    lastFragmentGoodFrames = skippedFrames;

//    skippedFrames = 0;
//    goodFrames = 0;

//    ///saving advert heatmap result
//    makeLogoDir(fragmentStartTime);
//    QString logoFilename = QString("%1%2/ad.png").arg(SmartOpt::getInstance()->dvrConfig.adModelConfig.adSavePath).arg(getAdModelFolderRelativePath(fragmentStartTime));

//    cv::imwrite( logoFilename.toStdString().c_str(), adHeat);
//    adHeat = Scalar(0,0,0);

//    ///saving pattern
//    makePatternDir(fragmentStartTime);
//    QString patternFilename = QString("%1%2/ad.pattern").arg(SmartOpt::getInstance()->dvrConfig.adModelConfig.patternSavePath).arg(getAdModelFolderRelativePath(fragmentStartTime));
//    patternHandler.closePattern(fragmentStartTime.toMSecsSinceEpoch(), recordCurrTimeDVR.toMSecsSinceEpoch());
//    patternHandler.writeToFile(patternFilename);
//    patternHandler.reset();
//}

//void Writer::finalizeFragment()
//{
//    //    qDebug() << "finalize fragment";
//    AVPacket * outputAudioPacket;
//    outputAudioPacket = new AVPacket();
//    av_init_packet(outputAudioPacket);
//    outputAudioPacket->data = NULL;
//    outputAudioPacket->size = 0;
//    int got_output;
//    bool ok = avcodec_encode_audio2(outputAudioCtx, outputAudioPacket, NULL, &got_output);
//    if (ok < 0 || !got_output){
//        if (ok < 0){
//            char *txt = new char[255];
//            av_strerror(ok,txt,255);
//            qDebug() << "Recorder::saveFrame could not encode audio packet [fail]"<< txt << " " << got_output ;
//            delete [] txt;
//        }
//    }else{

//        outputAudioPacket->stream_index = outputAudioStream->index;
//        av_packet_rescale_ts(outputAudioPacket, outputAudioStream->codec->time_base, outputAudioStream->time_base);
//        outputAudioPacket->duration =  av_rescale_q(outputAudioPacket->duration, outputAudioStream->codec->time_base, outputAudioStream->time_base);
//        int write_ret = av_write_frame(outputVideoFmtCtx, outputAudioPacket);
//        if (write_ret < 0) {
//            qDebug() << "Could not write to disk audio ";
//        }

//    }

//    AVPacket * outputVideoPacket;
//    outputVideoPacket = new AVPacket();
//    av_init_packet(outputVideoPacket);
//    outputVideoPacket->data = NULL;
//    outputVideoPacket->size = 0;
//    //    int got_output;
//    ok = avcodec_encode_video2(outputVideoCtx, outputVideoPacket, NULL, &got_output);
//    if (ok < 0 || !got_output){
//        if (ok < 0){
//            char *txt = new char[255];
//            av_strerror(ok,txt,255);
//            qDebug() << "Recorder::saveFrame could not encode video packet [fail]"<< txt << " " << got_output ;
//            delete [] txt;
//        }
//    }else{
//        outputVideoPacket->stream_index = outputVideoStream->index;
//        int write_ret = av_write_frame(outputVideoFmtCtx, outputVideoPacket);
//        if (write_ret < 0){
//            qDebug() << "Could not write to disk video ";
//        }

//    }

//    av_write_trailer(outputVideoFmtCtx);

//    av_free_packet(outputAudioPacket);
//    delete(outputAudioPacket);
//    av_free_packet(outputVideoPacket);
//    delete(outputVideoPacket);

//    emit fragmentDone(lastFragmentStartTime,lastFragmentEndTime,goodFrames,skippedFrames,fps);
//    if(status == WAITING_AUDIO_AND_END){
//        status = STOPPPING;
//    }else{
//        initOutputVideoFile(lastFragmentEndTime);
//        status = WRITING;

//        emit sourceStatusChange(currentSource.sourceId,currentSource.status,
//                                QString("Fragment saved [%1-%2]")
//                                .arg(lastFragmentStartTime.toString("hh.mm"))
//                                .arg(lastFragmentEndTime.toString("hh.mm")) );

//    }
//    //    if(fifo != 0) av_audio_fifo_reset(fifo);

//    return;
//}




//int Writer::select_sample_rate(AVCodec *codec)
//{
//    const int *p;
//    int best_samplerate = 0;

//    if (!codec->supported_samplerates){
//        return 44100;
//    }

//    p = codec->supported_samplerates;
//    while (*p) {
//        best_samplerate = FFMAX(*p, best_samplerate);
//        p++;
//    }
//    return best_samplerate;
//}


//int Writer::duplicateFrame(AVFrame *inputFrame, AVFrame ** outputFrame)
//{
//    *outputFrame = av_frame_clone(inputFrame);
//    return 0;
//}

