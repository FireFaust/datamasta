#ifndef WRITER_H
#define WRITER_H

#include <QObject>

extern "C" {
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
#include "libavutil/error.h"
#include "libavutil/frame.h"
#include "libavutil/audio_fifo.h"
#include "libavutil/channel_layout.h"
#include "libswresample/swresample.h"
#include "libavutil/intreadwrite.h"
}

class Writer:public QObject
{
    Q_OBJECT
public:
    explicit Writer(QObject * parent = 0);

public slots:
    void setup(int width, int height, QString filename);
    void writeFrame(uchar * frame, qint64 ts = -1);
    void finalize();
signals:
    void finished();
    void frameProcessed();
private:

    //For video
    AVCodecContext * outputVideoCtx;
    AVCodec * outputVideoCodec;

    AVOutputFormat * outputVideoFmt;
    AVFormatContext * outputVideoFmtCtx;
    AVStream * outputVideoStream;
    AVFrame * resizedVideoFrame;
    SwsContext * scaleCtx;

    AVFrame * outFrame;
    double outputPts;

    AVFrame * tFrame;
    AVPicture*picture;

    QString filename;
    int sourceWidth;
    int sourceHeight;

    int initOutputVideoFile();
    void freeOutputVideoFile();
    bool saveVideoFileFrame(AVFrame *inputFrame);

//private slots:
//    void run();
//    bool saveVideoFrame(AVFrame *inputFrame);
//    bool saveAudioFrame(AVFrame *inputFrame);

//private:
//    typedef enum {
//        IDLE,
//        STARTED,
//        WAITING_AUDIO,
//        WAITING_AUDIO_AND_END,
//        WAITING_WHOLE_SEC,
//        WAITING_START,
//        WRITING,
//        STOPPPING
//    } WritingStatus;

//    long processMemUsage(qint64 id);
//    //FrameCache *frameCache;
//    PacketQueue *packetQueue;

//    bool safeToReconnect;
//    inline void setSafeToReconnect(bool safeToReconnect){QMutexLocker locker(&reconnectMutex);this->safeToReconnect = safeToReconnect;}
//    mutable QMutex reconnectMutex;

//    /// Used to store raw video frames while waiting for audio to catch up. (status == WAITING_AUDIO)
//    VideoFrameQueue videoFrameQueue;

//    WritingStatus status;
//    bool mustTerminate;
//    MarkupDB::SourceInfo currentSource;

//    TVModel tvModel;

//    AVStream * inputVideoStream;
//    AVStream * inputAudioStream;

//    // For images
//    AVCodecContext * outputCtx;
//    AVCodec * outputCodec;
//    AVFrame * resizedThumbFrame;
//    AVPacket * outputPacket;

//    SwsContext * modelScaleCtx;
//    SwsContext * thumbScaleCtx;
//    AVFrame * outputModelFrame;
//    //    AVRational timeBase;

//    //For video
//    AVCodecContext * outputVideoCtx;
//    AVCodec * outputVideoCodec;

//    AVOutputFormat * outputVideoFmt;
//    AVFormatContext * outputVideoFmtCtx;
//    AVStream * outputVideoStream;

//    AVAudioFifo *fifo;
//    qint64 videoStartPTS;
//    qint64 audiStartTsDiff;
//    qint64 videoStartTS;
//    qint64 audioTSOffset;
//    qint64 audioStartTS;
//    qint64 outputPtsOffset;

//    bool hasOutputInitialised;
//    AVCodecContext * outputAudioCtx;
//    AVCodec * outputAudioCodec;
//    //    AVFrame * outputAudioFrame;
//    //    AVPacket * outputAudioPacket;

//    SwrContext *resample_context;

//    AVStream * outputAudioStream;
//    double lastAudioTs;
//    double lastAudioPtsOffset;



//    int thumbWidth;
//    int thumbHeight;

//    int videoWidth;
//    int videoHeight;

//    qint64 timeDiff;
//    qint64 currVideoFrameTs;
//    qint64 currAudioFrameTs;

//    qint64 fps;
//    qint64 frameDisplayTime;

//    qint64 goodFrames;
//    qint64 skippedFrames;
//    qint64 lastFragmentSkippedFrames;
//    qint64 lastFragmentGoodFrames;


//    qint64 frameNr;
//    qint64 recordStartTimeLastSecond;
//    qint64 framesInDoneFragments;
//    //! Forces deleyed record time set in #recordStartTimeDVR (absolute)
//    bool hasDelayedRecordStartTime;
//    //! Absolute record time. If #hasDelayedRecordStartTime == false then equals first recorded second
//    QDateTime recordStartTimeDVR;
//    bool hasRecordEndTime;
//    QDateTime recordEndTime;

//    QDateTime recordCurrTimeDVR;
//    QDateTime fragmentStartTime;
//    QDateTime lastFragmentStartTime;
//    QDateTime lastFragmentEndTime;

//    //! Stored time shift diff caused by looping source video - source video length + 1 frame length (ms)
//    qint64 dvrTsLoopCorrection;
//    qint64 audiPtsLoopCorrection;

//    qint64 wholeSecondsRecorded;
//    qint64 wholeFragmentsRecorded;
//    qint64 fragmentShift;
//    qint64 fragmentLength;
//    qint64 nextAudioPts;

//    //    qint64 startPTS;

//    bool droppedFrame;
//    int droppedFrameCount;
//    qint64 nextPts;
//    qint64 nextDts;

//    void disconnect();

//    bool decodeVideoFrame(AVPacket *inputPacket);
//    bool decodeAudioFrame(AVPacket *inputPacket);

//    bool makeSecDir(QDateTime time);
//    QString getSecondFolderRelativePath(QDateTime time);

//    bool makeAdModelDir(QDateTime time);
//    bool makeLogoDir(QDateTime time);
//    bool makePatternDir(QDateTime time);
//    QString getAdModelFolderRelativePath(QDateTime time);


//    void finalizeModel();
//    void finalizeFragment();

//    ///
//    /// \brief initOutputVideoFile
//    /// \param startMinute used for filename of video file
//    /// \return
//    ///

//    static int select_sample_rate(AVCodec *codec);

//    int duplicateFrame(AVFrame *inputFrame, AVFrame ** outputFrame);

//    /// Variables needed for advert detection
//    LogoHandler * logoDetect;
//    int logoConfidence;
//    int * logoPresence;
//    Mat adHeat;

//    qint64 audioOffsetForFrameDrop = 0;

//    PatternHandler patternHandler;
};

#endif // WRITER_H
